from django.db import models

import os
import uuid

from django.core.validators import MaxValueValidator, MinValueValidator

# Create your models here.
def manufacturer_directory_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = '{}.{}'.format(uuid.uuid4().hex[:10], ext)
    return os.path.join("manufacturer", filename)

def asset_directory_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = '{}.{}'.format(uuid.uuid4().hex[:10], ext)
    return os.path.join("asset", filename) 

def consumable_directory_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = '{}.{}'.format(uuid.uuid4().hex[:10], ext)
    return os.path.join("consumable", filename)   

def accessorie_directory_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = '{}.{}'.format(uuid.uuid4().hex[:10], ext)
    return os.path.join("accessorie", filename) 



Asset_CHOICES = [
    (1 , 'Computer'),
    (2 , 'Laptop'),
    (3 , 'Printer'), 
    (4 , 'Switch') ,
    (5 , 'Access Point'),
    (6 , 'Monitor'),
]

Brand_CHOICES = [
    ('Acer' , 'Acer'),
    ('ASUS' , 'ASUS'),
    ('Complex PC' , 'Complex PC'),
    ('IBM' , 'IBM'),
    ('HP' , 'HP'),
    ('Lenovo' , 'Lenovo'),
    ('Dell' , 'Dell'),    
    ('Brother' , 'Brother'),
    ('Epson' , 'Epson'),            
]

AssetStatus_CHOICES = [
    (0, 'Damage'),
    (1, 'Ready'),
]

class ItemcodeMaster(models.Model):
    Category_CHOICES = [
    (1 , 'Asset'),
    (2 , 'License'),
    (3 , 'Accessorie'),
    (4 , 'Consumable'),
    (5 , 'Component'),            
]
    item_code = models.CharField(max_length=50, unique=True, blank=False, null=False)
    item_name = models.CharField(max_length=200, unique=True, blank=False, null=False)
    category = models.IntegerField(blank=True,null=True, choices=Category_CHOICES)

    def __str__(self):
        return self.item_name

    def category_choices_value(self):
        return dict(self.Category_CHOICES)[self.category]

class Department(models.Model):
    department_name = models.CharField(max_length=200)	

    def __str__(self):
        return self.department_name

StatusUser_CHOICES = [
    (0 , 'ลาออก'),
    (1 , 'ทำงาน'),
]

class List_Of_Software(models.Model):
    name = models.CharField(max_length=200, unique=True, blank=False, null=False)

    def __str__(self):
        return self.name

class People(models.Model):

    Division_CHOICES = [
        (1, 'MFG'),
        (2, 'SC'),
        (3, 'BD'),
        (4, 'Finance'),
        (5, 'OD'),
        (6, 'R&D'),

    ]

    username = models.CharField(max_length=64, unique=True, blank=False, null=False)
    first_name = models.CharField(max_length=200, blank=False, null=False)
    last_name = models.CharField(max_length=200, blank=False, null=False)
    position = models.CharField(max_length=200, blank=True, null=True)
    department = models.ForeignKey(Department,blank=True, null=True, on_delete=models.PROTECT)
    division = models.IntegerField(choices=Division_CHOICES, blank=True, null=True)
    email = models.CharField(max_length=100,blank=True, null=True)
    status = models.IntegerField(blank=False,null=False, choices=StatusUser_CHOICES)
    created_at = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return '%s %s'%(self.first_name, self.last_name)
        # return self.first_name + ' ' + self.last_name  + ' (' + self.department.department_name + ')'
    def division_choices_value(self):
        if self.division == None:
            return ''
        else:
            return dict(self.Division_CHOICES)[self.division]

class Depreciation(models.Model):
    depreciation_name = models.CharField(max_length=200,blank=False,null=False)
    number_of_month = models.IntegerField(blank=False,null=False)

    def __str__(self):
        return self.depreciation_name

class Location(models.Model):
    location_name = models.CharField(max_length=100, blank=False, null=False)
    sub_location_name = models.CharField(max_length=100, blank=False, null=False)    

    class Meta:
        unique_together = ('location_name', 'sub_location_name',)

    def __str__(self):
        return self.location_name + ' (' + self.sub_location_name + ')'


class Supplier(models.Model):
    TermOfPayment_CHOICES = [
        (1, '15D'),
        (2, '30D'),
        (3, '45D'),
        (4, '60D'),
        (5, '75D'),
        (6, '7D'),
        (7, '90D'),
        (8, 'Cash'),

    ]
    supplier_name = models.CharField(max_length=200, blank=False, null=False)
    address = models.TextField(max_length=300, blank=True, null=True)
    mobile_phone = models.CharField(max_length=35, blank=True, null=True)
    office_phone = models.CharField(max_length=35, blank=True, null=True)
    fax = models.CharField(max_length=35, blank=True, null=True)
    email = models.CharField(max_length=150, blank=True, null=True)
    term_of_payment = models.IntegerField(blank=True, null=True, choices=TermOfPayment_CHOICES )

    def __str__(self):
        return self.supplier_name

    def termofpayment_choices_value(self):
        return dict(self.TermOfPayment_CHOICES)[self.term_of_payment]

check_in_out_CHOICE = [
        (0, 'Out'),
        (1, 'Stock'),
]

class Asset(models.Model):

    AssetStatus_CHOICES = (
    (0, 'Damage'),
    (1, 'Ready'),
)

    Asset_CHOICES = (
        (1 , 'Desktop Computer'),
        (2 , 'Laptop Computer'),
        (3 , 'Printer'), 
        (4 , 'Switch') ,
        (5 , 'Access Point'),
        (6 , 'Monitor'),
    )

    TypeLocation_CHOICE = (
        (1 , 'Production'),
        (2 , 'Operation'),
        (3 , 'Office'),
    )

    Frequency_CHOICE = (
        (1 , '1 Month'),
        (2 , '2 Month'),
        (3 , '3 Month'),
        (6 , '6 Month'),
        (12 , '1 Year'),
    )

    

    item_code = models.ForeignKey(ItemcodeMaster, null=False, blank=False, on_delete=models.PROTECT)
    asset_type = models.IntegerField(choices=Asset_CHOICES, blank=True, null=True)

    asset_no = models.CharField(max_length=100, blank=True, null=True,unique=True)
    model = models.CharField(max_length=100, blank=True, null=True)
    serial =models.CharField(max_length=100, blank=True, null=True)
    brand = models.CharField(max_length=100, blank=True, null=True, choices=Brand_CHOICES)
    asset_name = models.CharField(max_length=100, blank=True, null=True)

    location = models.ForeignKey(Location, null=False, blank=False, on_delete=models.PROTECT)
    ip_address_1 = models.CharField(max_length=15, blank=True, null=True)
    ip_address_2 = models.CharField(max_length=15, blank=True, null=True)
    mac_address_lan_1 = models.CharField(max_length=16, blank=True, null=True)
    mac_address_lan_2 = models.CharField(max_length=16, blank=True, null=True)
    mac_address_wifi = models.CharField(max_length=16, blank=True, null=True)    

    suppiler =  models.ForeignKey(Supplier,blank=True, null=True, on_delete=models.PROTECT)
    purchase_date = models.DateField(blank=True, null=True)
    purchase_cost = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    invoice_number = models.CharField(max_length=191, blank=True, null=True)
    receive_date = models.DateField(blank=True, null=True)

    warranty_startdate = models.DateField(blank=True, null=True)
    warranty_expiredate = models.DateField(blank=True, null=True)

    status = models.IntegerField(choices=AssetStatus_CHOICES,blank=False, null=False,default=1)
    check_in_out = models.IntegerField(choices=check_in_out_CHOICE, blank=True, null=True, default=1)

    image = models.FileField(upload_to=asset_directory_path, null=True, blank=True)
    fix_asset_no = models.CharField(max_length=100, blank=True, null=True)
    type_location = models.IntegerField(choices=TypeLocation_CHOICE,blank=False, null=False)
    frequency_pm = models.IntegerField(choices=Frequency_CHOICE,blank=True, null=True)
    pm_date_next = models.DateField(blank=True, null=True)

    importance_score = models.SmallIntegerField(blank=True, null=True, default=1, 
        validators=[MinValueValidator(1), MaxValueValidator(3)])
    servicelife_score = models.SmallIntegerField(blank=True, null=True, 
        validators=[MinValueValidator(1), MaxValueValidator(3)])
    environment_score = models.SmallIntegerField(blank=True, null=True, 
        validators=[MinValueValidator(1), MaxValueValidator(3)])
    
    # created_by = models.ForeignKey(User, null=False, blank=False, on_delete=models.PROTECT)
    # created_at = models.DateTimeField(blank=True, null=True)
    # updated_by = models.ForeignKey(User, null=False, blank=False, on_delete=models.PROTECT)
    # updated_at = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return '%s - %s'%(self.asset_name, self.serial)

    def assetstatus_type(self):
        return dict(self.AssetStatus_CHOICES)[self.status]

    def asset_choices(self):
        return dict(self.Asset_CHOICES)[self.asset_type]

    def type_location_value(self):
        return dict(self.TypeLocation_CHOICE)[self.type_location]

    def frequency_pm_value(self):
        return dict(self.Frequency_CHOICE)[self.frequency_pm]

    def status_check_in_out_value(self):
        return (check_in_out_CHOICE)[self.check_in_out][1]

class AssetIssue(models.Model):
    asset_id =  models.ForeignKey(Asset,blank=True, null=True, on_delete=models.PROTECT) 
    user_id =  models.ForeignKey(People,blank=True, null=True, on_delete=models.PROTECT) 
    checked_date = models.DateField(blank=True, null=True)

class LogCheckinout(models.Model):
    asset = models.ForeignKey(Asset,blank=True, null=True, on_delete=models.PROTECT)
    log_date = models.DateTimeField(blank=True, null=True)
    status = models.IntegerField(choices=check_in_out_CHOICE, blank=True, null=True)

class AssetSoftware(models.Model):
    asset_id = models.ForeignKey(AssetIssue,blank=True, null=True, on_delete=models.PROTECT)
    software_id = models.ForeignKey(List_Of_Software,blank=True, null=True, on_delete=models.PROTECT)

class Manufacturer(models.Model):
    name = models.CharField(max_length=200,blank=False,null=False)
    support_url = models.CharField(max_length=100, blank=True, null=True)
    support_phone = models.CharField(max_length=35, blank=True, null=True)
    support_email = models.CharField(max_length=100,blank=True,null=True)
    image = models.FileField(upload_to=manufacturer_directory_path, null=True, blank=True)

    def __str__(self):
        return self.name 

class Licenses(models.Model):

    CategorieSoftware_CHOICES = (
        (1, 'Operating software'),
        (2, 'Office software'),
        (3, 'Anti-virus software'),
        (4, 'Aided desige software'),
        (5, 'Graphics software'),
        (6, 'Programming software'),
        (7, 'Other'),   
    )

    software_name = models.CharField(max_length=200,blank=False,null=False)
    software_version = models.CharField(max_length=100,blank=False,null=False)
    software_categorie = models.IntegerField(choices=CategorieSoftware_CHOICES, blank=False, null=False)
    product_key =  models.CharField(max_length=200, blank=True, null=True)
    qty = models.IntegerField()

    manufacturer = models.ForeignKey(Manufacturer,blank=True,null=True, on_delete=models.PROTECT)
    licensed_to_name = models.CharField(max_length=150, blank=True, null=True)
    licensed_to_email = models.CharField(max_length=150, blank=True, null=True)

    suppiler =  models.ForeignKey(Supplier,blank=True, null=True, on_delete=models.PROTECT)
    purchase_cost = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    invoice_number = models.CharField(max_length=191, blank=True, null=True)
    receive_date = models.DateField(blank=True, null=True)

    warranty_startdate = models.DateField(blank=False, null=False)
    warranty_expiredate = models.DateField(blank=False, null=False)

    depreciation = models.ForeignKey(Depreciation,blank=True, null=True, on_delete=models.PROTECT)
    notes = models.TextField(max_length=200,blank=True, null=True)

    def software_categorie_type(self):
        return dict(self.CategorieSoftware_CHOICES)[self.software_categorie]


class Consumable(models.Model):

    CategorieConsumable_CHOICES = (
        (0, 'Laser Printer'),
        (1, 'Ink Jet Printer'),
        (2, 'UTP Cable'),
        (3, 'Mainternance Printer'),
    )

    status_CHOICE = (
        (0, 'Receive'),
        (1, 'BF'),
    )

    itemcode = models.ForeignKey(ItemcodeMaster,blank=False, null=False, on_delete=models.PROTECT)
    consumable_name = models.CharField(max_length=200,blank=False,null=False)
    consumable_catagory = models.IntegerField(choices=CategorieConsumable_CHOICES, blank=False, null=False)
    manufacturer = models.ForeignKey(Manufacturer,blank=True,null=True, on_delete=models.PROTECT)

    location = models.ForeignKey(Location, null=False, blank=False, on_delete=models.PROTECT)

    brand = models.CharField(max_length=100, blank=True, null=True, choices=Brand_CHOICES)
    model = models.CharField(max_length=100, blank=True, null=True)
    serial =models.CharField(max_length=100, blank=True, null=True)

    suppiler =  models.ForeignKey(Supplier,blank=True, null=True, on_delete=models.PROTECT)
    purchase_cost = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    invoice_number = models.CharField(max_length=191, blank=True, null=True)
    receive_date = models.DateField(blank=True, null=True)

    warranty_startdate = models.DateField(blank=True, null=True)
    warranty_expiredate = models.DateField(blank=True, null=True)

    qty = models.IntegerField()
    min_qty = models.IntegerField()
    image = models.FileField(upload_to=consumable_directory_path, null=True, blank=True)
    status = models.IntegerField(choices=status_CHOICE, blank=True, null=True,default=0)
    
    def consumable_catagory_value(self):
        return dict(self.CategorieConsumable_CHOICES)[self.consumable_catagory]   #dist ตรง Choice จะเป็น (...)

        #return list(self.CategorieConsumable_CHOICES)[self.consumable_catagory][1]  list แต่ตรง Choice จะเป็น [...] และต้องเริ่มต้นด้วย 0

class ConsumablesIssue(models.Model):
    department_id = models.ForeignKey(Department,blank=True, null=True, on_delete=models.PROTECT)
    assigned_qty = models.IntegerField()
    consumable_id = models.ForeignKey(Consumable,blank=True, null=True, on_delete=models.PROTECT)
    created_at = models.DateField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)

class Accessorie(models.Model):
    status_CHOICE = (
        (0, 'Receive'),
        (1, 'BF'),
    )

    itemcode = models.ForeignKey(ItemcodeMaster,blank=False, null=False, on_delete=models.PROTECT)
    accessorie_name = models.CharField(max_length=200,blank=False,null=False)
    manufacturer = models.ForeignKey(Manufacturer,blank=True,null=True, on_delete=models.PROTECT)
    location = models.ForeignKey(Location, null=False, blank=False, on_delete=models.PROTECT)

    brand = models.CharField(max_length=100, blank=True, null=True, choices=Brand_CHOICES)
    model = models.CharField(max_length=100, blank=True, null=True)
    serial =models.CharField(max_length=100, blank=True, null=True)

    suppiler =  models.ForeignKey(Supplier,blank=True, null=True, on_delete=models.PROTECT)
    purchase_cost = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    invoice_number = models.CharField(max_length=191, blank=True, null=True) 
    receive_date = models.DateField(blank=True, null=True)

    warranty_startdate = models.DateField(blank=True, null=True)
    warranty_expiredate = models.DateField(blank=True, null=True)

    qty = models.IntegerField()
    min_qty = models.IntegerField()
    image = models.FileField(upload_to=accessorie_directory_path, null=True, blank=True)
    status = models.IntegerField(choices=status_CHOICE, blank=True, null=True,default=0)

    def __str__(self):
        return '%s : %s - %s' %(self.accessorie_name, self.brand, self.model)

class AccessoriesIssue(models.Model):
    people_id = models.ForeignKey(People,blank=True, null=True, on_delete=models.PROTECT)
    assigned_qty = models.IntegerField()
    accessorie_no = models.CharField(max_length=100, blank=True, null=True)
    accessorie_id = models.ForeignKey(Accessorie,blank=True, null=True, on_delete=models.PROTECT)
    created_at = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.people_id__first_name

class Component(models.Model):
    status_CHOICE = (
        (0, 'Receive'),
        (1, 'BF'),
    )

    itemcode = models.ForeignKey(ItemcodeMaster,blank=False, null=False, on_delete=models.PROTECT)
    component_name = models.CharField(max_length=200,blank=False,null=False)
    manufacturer = models.ForeignKey(Manufacturer,blank=True,null=True, on_delete=models.PROTECT)
    location = models.ForeignKey(Location, null=False, blank=False, on_delete=models.PROTECT)

    brand = models.CharField(max_length=100, blank=True, null=True, choices=Brand_CHOICES)
    model = models.CharField(max_length=100, blank=True, null=True)
    serial = models.CharField(max_length=100, blank=True, null=True)

    suppiler =  models.ForeignKey(Supplier,blank=True, null=True, on_delete=models.PROTECT)
    purchase_cost = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    invoice_number = models.CharField(max_length=191, blank=True, null=True)
    receive_date = models.DateField(blank=True, null=True)

    warranty_startdate = models.DateField(blank=True, null=True)
    warranty_expiredate = models.DateField(blank=True, null=True)

    qty = models.IntegerField()
    min_qty = models.IntegerField()
    image = models.FileField(upload_to=manufacturer_directory_path, null=True, blank=True)
    status = models.IntegerField(choices=status_CHOICE, blank=True, null=True,default=0)

class ComponentsIssue(models.Model):
    people_id = models.ForeignKey(People,blank=True, null=True, on_delete=models.PROTECT)
    assigned_qty = models.IntegerField()
    component_id = models.ForeignKey(Component,blank=True, null=True, on_delete=models.PROTECT)
    created_at = models.DateField(blank=True, null=True)

class AssetMaintenance(models.Model):
    AssetMaintenance_CHOICES =[
    (1, 'Upgrade'),
    (2, 'Maintenance'),
    (3, 'Repair'),
    (4, 'Software Support'),
    (5, 'Calibration'),
    ]

    asset_no = models.ForeignKey(Asset,blank=True, null=True, on_delete=models.PROTECT, verbose_name='Asset No')
    location = models.ForeignKey(Location, null=False, blank=False, on_delete=models.PROTECT)
    maintenance_type = models.IntegerField(choices=AssetMaintenance_CHOICES, blank=False, null=False, verbose_name='Maintenance Type')

    title = models.CharField(verbose_name='Title', max_length=200, blank=True, null=True)
    start_date = models.DateField(blank=True, null=True)
    compietion_date = models.DateField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    waranty = models.IntegerField(blank=True, null=True)
    cost = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)

    def maintenance_type_value(self):
        return dict(self.AssetMaintenance_CHOICES)[self.maintenance_type]

class SoftwareForAsset(models.Model):
    asset = models.ForeignKey(Asset,blank=True, null=True, on_delete=models.PROTECT, verbose_name='Asset No')
    os_name = models.BooleanField(blank=True, null=True)
    os_std = models.ForeignKey(List_Of_Software,  related_name='os_std', null=False, blank=False, on_delete=models.PROTECT)
    office = models.BooleanField(blank=True, null=True)
    office_std = models.ForeignKey(List_Of_Software, related_name='office_std', null=False, blank=False, on_delete=models.PROTECT)
    browser = models.BooleanField(blank=True, null=True)
    browser_std = models.CharField(max_length=200, blank=True, null=True)
    email = models.BooleanField(blank=True, null=True)
    email_std = models.CharField(max_length=200, blank=True, null=True)
    helpdesk = models.BooleanField(blank=True, null=True)
    helpdesk_std = models.CharField(max_length=200, blank=True, null=True)
    lineapp = models.BooleanField(blank=True, null=True)
    line_std = models.CharField(max_length=200, blank=True, null=True)
    antivirus = models.BooleanField(blank=True, null=True)
    antivirus_std = models.ForeignKey(List_Of_Software, related_name='antivirus_std', null=False, blank=False, on_delete=models.PROTECT)
    mta = models.BooleanField(blank=True, null=True)
    ax = models.BooleanField(blank=True, null=True)

    software_Special1 = models.ForeignKey(List_Of_Software,related_name='software_Special1', null=True, blank=True, on_delete=models.PROTECT)
    special_check1 = models.BooleanField(blank=True, null=True)
    program_name1 = models.CharField(max_length=200, blank=True, null=True)

    software_Special2 = models.ForeignKey(List_Of_Software,related_name='software_Special2', null=True, blank=True, on_delete=models.PROTECT)
    special_check2 = models.BooleanField(blank=True, null=True)
    program_name2 = models.CharField(max_length=200, blank=True, null=True)

    software_Special3 = models.ForeignKey(List_Of_Software,related_name='software_Special3', null=True, blank=True, on_delete=models.PROTECT)
    special_check3 = models.BooleanField(blank=True, null=True)
    program_name3 = models.CharField(max_length=200, blank=True, null=True)

    software_Special4 = models.ForeignKey(List_Of_Software,related_name='software_Special4', null=True, blank=True, on_delete=models.PROTECT)
    special_check4 = models.BooleanField(blank=True, null=True)
    program_name4 = models.CharField(max_length=200, blank=True, null=True)

    software_Special5 = models.ForeignKey(List_Of_Software,related_name='software_Special5', null=True, blank=True, on_delete=models.PROTECT)
    special_check5 = models.BooleanField(blank=True, null=True)
    program_name5 = models.CharField(max_length=200, blank=True, null=True)

    mouse = models.BooleanField(blank=True, null=True)
    keyboard = models.BooleanField(blank=True, null=True)
    adaptor = models.BooleanField(blank=True, null=True)
    gigbag = models.BooleanField(blank=True, null=True)

class PreventiveComputerSuccess(models.Model):
    HardwareStatus_CHOICES =[
    (1, 'Normal'),
    (2, 'Abnormal'),
    ]

    Comment_CHOICES =[
    (1, 'Preventive maintenance by dust cleaning and installed new OS'),
    (2, 'Preventive maintenance by dust cleaning'),
    ]


    asset_id = models.ForeignKey(Asset,blank=True, null=True, on_delete=models.PROTECT, verbose_name='Asset No')
    pm_date = models.DateField(null=False, blank=False)
    pm_by = models.ForeignKey(People,blank=True, null=True, on_delete=models.PROTECT)
    comment_pm = models.IntegerField(choices=Comment_CHOICES, blank=False, null=False, verbose_name='Comment')
    os = models.CharField(max_length=100, blank=True, null=True)
    health_status = models.IntegerField(choices=HardwareStatus_CHOICES, blank=False, null=False, verbose_name='hardware health status')
    remark = models.CharField(max_length=200, blank=True, null=True)

    def hardware_status_value(self):
        return dict(self.HardwareStatus_CHOICES)[self.health_status]

    def comment_value(self):
        return dict(self.Comment_CHOICES)[self.comment_pm] 

class Leases(models.Model):

    LeasesType_CHOICES =[
    (1 , 'Printer paper'),
    (2 , 'Licenses'),
    (3 , 'Lease Line'),
    (4 , 'Domain'),
    ]

    leases_type = models.IntegerField(choices=LeasesType_CHOICES, blank=False, null=False, verbose_name='Leases Type')
    leases_name = models.CharField(max_length=200, blank=True, null=True)
    description = models.CharField(max_length=2000, blank=True, null=True)

    document_no = models.CharField(max_length=100, blank=True, null=True)
    seriel_number = models.CharField(max_length=100, blank=True, null=True)
    qty = models.IntegerField()
    owner_email = models.CharField(max_length=100, blank=True, null=True)
    contract_period = models.IntegerField()

    manufacturer = models.ForeignKey(Manufacturer,blank=True,null=True, on_delete=models.PROTECT)
    suppiler =  models.ForeignKey(Supplier,blank=True, null=True, on_delete=models.PROTECT)
    purchase_cost = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    po_no = models.CharField(max_length=191, blank=True, null=True)
    receive_date = models.DateField(blank=True, null=True) 

    warranty_startdate = models.DateField(blank=True, null=True)
    warranty_expiredate = models.DateField(blank=True, null=True)

    def leasestype_choices(self):
        return dict(self.LeasesType_CHOICES)[self.leases_type]

class BFstock(models.Model):
    itemcode = models.ForeignKey(ItemcodeMaster,blank=False, null=False, on_delete=models.PROTECT)
    qty = models.IntegerField()
    bf_date = models.DateField(blank=True, null=True)
    create_at = models.DateTimeField(blank=True, null=True)



