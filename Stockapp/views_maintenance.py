from django.shortcuts import render, redirect , get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.db.models import Avg, Max, Min, Sum

from .models import AssetMaintenance, Asset, Location
from .forms import MaintenanceCreateForm, MaintenanceViewForm

def viewallmaintenance(request):
    maintenances = AssetMaintenance.objects.all()
    detailmaintenance = []

    for item in maintenances:    
        # combine
        detailmaintenance.append({
            'id': item.id,
            'maintenance_type': item.maintenance_type,   
            'title': item.title,
            'start_date': item.start_date,
            'compietion_date': item.compietion_date,
            'notes': item.notes,
            'waranty': item.waranty,
            'cost' : item.cost,
            'asset_no' : item.asset_no,       
            'maintenance_type_value': item.maintenance_type_value,
        })
    return render(request, 'stockapp/allmaintenance.html', {'pageTitle': 'All Asset Maintenance', 'maintenances': detailmaintenance})

def createmaintenance(request):
    if request.method == 'GET':
        return render(request, 'stockapp/create_maintenance.html',
        {            
            'form': MaintenanceCreateForm(),
            'pageTitle': 'Create new Maintenance'
        })
    else:
        try:
            form = MaintenanceCreateForm(request.POST, request.FILES)
            if form.is_valid():
                newdata = form.save(commit=False)
                newdata.request_user = request.user
                newdata.save()
                return redirect('viewallmaintenance')                
            else:
                return render(request, 'stockapp/create_maintenance.html',
                {
                    'form': MaintenanceCreateForm(),
                     'error': form.errors
                })

        except ValueError as error:
            return render(request, 'stockapp/create_maintenance.html', {'form': MaintenanceCreateForm(), 'error': error}) 

def viewmaintenancedetail(request, maintenance_pk):
    item = get_object_or_404(AssetMaintenance, pk=maintenance_pk)

    if request.method == 'GET':
        form = MaintenanceViewForm(instance=item)
        return render(request, 'stockapp/viewmaintenance.html',
        {
            'pageTitle': 'View Asset Maintenance Detail',
            'item': item,
            'form': form
        })
    else:
        try:
            form = MaintenanceViewForm(request.POST, instance=item)
            form.save()
            return redirect('viewallmaintenance')
        except ValueError as error:
            return render(request, 'stockapp/viewmaintenance.html',
            {
                'pageTitle': 'View Asset Maintenance Detail',
                'item': item, 
                'form': form, 
                'error': error
            })