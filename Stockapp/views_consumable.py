from django.shortcuts import render, redirect , get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.db.models import Avg, Max, Min, Sum

from .models import (Consumable,ItemcodeMaster,ConsumablesIssue)
from .forms import (ConsumableCreateForm,ConsumableViewForm,ConsumableIssueForm)

def createconsumable(request):
    if request.method == 'GET':

        consumeitems = ItemcodeMaster.objects.filter(category=4)

        return render(request, 'stockapp/createconsumable.html', 
        {
            'form': ConsumableCreateForm(),
            'pageTitle': 'Create new Consumable (วัสดุสิ้นเปลือง)',
            'consumeitems': consumeitems
        })
    else:
        try:
            form = ConsumableCreateForm(request.POST, request.FILES)
            if form.is_valid():
                newdata = form.save(commit=False)

                iteminstant = ItemcodeMaster.objects.get(id=request.POST['itemcode'])
                newdata.itemcode = iteminstant
                
                newdata.request_user = request.user
                newdata.save()
                return redirect('viewallconsumable')                
            else:
                return render(request, 'stockapp/createconsumable.html', {'form': ConsumableCreateForm(), 'error': form.errors})

        except ValueError as error:
            return render(request, 'stockapp/createconsumable.html', {'form': ConsumableCreateForm(), 'error': error}) 

def viewallconsumable(request):
    consumables = Consumable.objects.all()

    sub_consumable_obj = ConsumablesIssue.objects.values('consumable_id').annotate(sum=Sum('assigned_qty')) # Query หนเดียวแล้ว*
    detailconsumable = []

    for item in consumables:    
        # combine
        
        consumable_qty = 0 #* ใช้ Loop เพื่อหา Sum ด้วย id
        for i in sub_consumable_obj: # find sum in QuerySet
            if (item.id == i['consumable_id']):
                consumable_qty = i['sum']
                break

        # combine
        detailconsumable.append({
            'id': item.id,
            'itemcode': item.itemcode,
            'consumable_name': item.consumable_name,
            #'consumable_catagory': item.consumable_catagory,
            'consumable_catagory_value': item.consumable_catagory_value,
            'brand': item.brand,
            'model': item.model,
            'qty': item.qty,

            'consumable_qty' : consumable_qty,
            'balance' : item.qty - consumable_qty,

            'suppiler' : item.suppiler,
            
        })

    return render(request, 'stockapp/allconsumable.html', {'pageTitle': 'All Consumables (วัสดุสิ้นเปลือง)', 'consumables': detailconsumable}) 

def viewconsumable(request, consumable_pk):    

    item = get_object_or_404(Consumable, pk=consumable_pk)

    if request.method == 'GET':
        form = ConsumableViewForm(instance=item)
        return render(request, 'stockapp/viewconsumable.html', {'pageTitle': 'View Consumable Detail', 'item': item, 'form': form})
    else:
        try:
            form = ConsumableViewForm(request.POST, instance=item)
            form.save()
            return redirect('viewallconsumable')
        except ValueError:
            return render(request, 'stockapp/viewconsumable.html', {'pageTitle': 'View Consumable Detail', 'item': item, 'form': form, 'error': "Bad info."})

def IssueConsumableUser(request, consumable_pk):
    item = get_object_or_404(Consumable, pk=consumable_pk)

    if request.method == 'GET':
        item_name_obj = Consumable.objects.get(id = consumable_pk)
        return render(request, 'stockapp/issue_consumable.html', 
        {
            'form': ConsumableIssueForm(),
            'pageTitle': 'Issue Consumable Form (เบิกวัสดุสิ้นเปลือง)',
            'item_code': item_name_obj.itemcode
        })
    else:
        try:
            form = ConsumableIssueForm(request.POST)
            if form.is_valid():
                newdata = form.save(commit=False)

                instance_Consumable = Consumable.objects.get(id = consumable_pk)
                newdata.consumable_id = instance_Consumable

                newdata.created_at = timezone.now()
                newdata.save()
                return redirect('viewallconsumable')                
            else:
                return render(request, 'stockapp/issue_consumable.html', {'form': ConsumableIssueForm(), 'error': form.errors})

        except ValueError as error:
            return render(request, 'stockapp/issue_consumable.html', {'form': ConsumableIssueForm(), 'error': error})    
