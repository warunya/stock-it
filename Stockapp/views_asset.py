from django.shortcuts import render, redirect , get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.db.models import Avg, Max, Min, Sum

from django.http import Http404

from .models import Asset,ItemcodeMaster,AssetIssue,LogCheckinout
from .forms import (AssetViewForm,AssetCreateForm,AssetIssueForm,SoftwareAssetForm)

def createasset(request):
    if request.method == 'GET':

        assetitems = ItemcodeMaster.objects.filter(category=1)

        return render(request, 'stockapp/createasset.html', {
            'form': AssetCreateForm(),
            'pageTitle': 'Create new Asset.',
            'assetitems': assetitems
        })
    else:
        try:
            form = AssetCreateForm(request.POST, request.FILES)
            if form.is_valid():
                newdata = form.save(commit=False)
                
                iteminstant = ItemcodeMaster.objects.get(id=request.POST['itemcode'])
                newdata.itemcode = iteminstant
                newdata.request_user = request.user
                newdata.save()
                return redirect('viewallasset')                
            else:
                return render(request, 'stockapp/createasset.html', {'form': AssetCreateForm(), 'error': form.errors})

        except ValueError as error:
            return render(request, 'stockapp/createasset.html', {'form': AssetCreateForm(), 'error': error})  

def viewallasset(request):
    assets = Asset.objects.all()
    return render(request, 'stockapp/allasset.html', {'pageTitle': 'All Asset', 'assets': assets}) 

def viewallcomputer(request):
    computers = Asset.objects.filter(asset_type=1)
    return render(request, 'stockapp/allcomputers.html', {'pageTitle': 'All Computer', 'computers': computers}) 

def viewalllaptop(request):
    laptops = Asset.objects.filter(asset_type=2)
    return render(request, 'stockapp/alllaptops.html', {'pageTitle': 'All Laptop', 'laptops': laptops}) 

def viewallprinter(request):
    printers = Asset.objects.filter(asset_type=3)
    return render(request, 'stockapp/allprinters.html', {'pageTitle': 'All Printer', 'printers': printers}) 

def viewallswitch(request):
    switch = Asset.objects.filter(asset_type=4)
    return render(request, 'stockapp/allswitch.html', {'pageTitle': 'All Switch', 'switch': switch}) 

def viewallaccesspoint(request):
    accesspoint = Asset.objects.filter(asset_type=5)
    return render(request, 'stockapp/allswitch.html', {'pageTitle': 'All Access Point', 'accesspoint': accesspoint})

def viewallmonitor(request):
    monitors = Asset.objects.filter(asset_type=6)
    return render(request, 'stockapp/allswitch.html', {'pageTitle': 'All Monitor', 'monitors': monitors})

def viewasset(request, asset_pk):    

    item = get_object_or_404(Asset, pk=asset_pk)

    if request.method == 'GET':
        form = AssetViewForm(instance=item)
        return render(request, 'stockapp/viewasset.html', {'pageTitle': 'View Asset Detail', 'item': item, 'form': form})
    else:
        try:
            form = AssetViewForm(request.POST, instance=item)
            form.save()
            return redirect('viewallasset')
        except ValueError as error:
            return render(request, 'stockapp/viewasset.html', {'pageTitle': 'View Asset Detail', 'item': item, 'form': form, 'error': error}) 

def IssueAssetUser(request, asset_pk):
    item = get_object_or_404(Asset, pk=asset_pk)

    if request.method == 'GET':
        item_name_obj = ItemcodeMaster.objects.get(id = asset_pk)
        asset_name_obj = Asset.objects.get(id = asset_pk)
        return render(request, 'stockapp/issue_asset.html', 
        {
            'form': AssetIssueForm(),
            'pageTitle': 'Issue Asset Form (Fixed Assets)',
            'item_name': asset_name_obj.item_code,
            'asset_name': asset_name_obj.asset_name,
            'brand': asset_name_obj.brand,
            'serial': asset_name_obj.serial,
            'model': asset_name_obj.model,
            'asset_no': asset_name_obj.asset_no,
            'asset_type': asset_name_obj.asset_type,
            'serial': asset_name_obj.serial,
        })
    else:
        try:
            form = AssetIssueForm(request.POST)
            if form.is_valid():
                # insert data in AssetIssue
                newdata = form.save(commit=False)

                instance_Asset = Asset.objects.get(id = asset_pk)
                newdata.asset_id = instance_Asset
                newdata.save()

                # update status Check in_out
                instance_Asset.check_in_out = 0
                instance_Asset.save()

                return redirect('viewallasset')                
            else:
                return render(request, 'stockapp/issue_asset.html', {'form': AssetIssueForm(), 'error': form.errors})

        except ValueError as error:
            return render(request, 'stockapp/issue_asset.html', {'form': AssetIssueForm(), 'error': error})  

def AssetForSoftware(request, asset_pk):
    item = get_object_or_404(Asset, pk=asset_pk)

    if request.method == 'GET':
        item_name_obj = ItemcodeMaster.objects.get(id = asset_pk)
        # asset_name_obj = Asset.objects.get(id = asset_pk)
        asset_name_obj = get_object_or_404(Asset, id = asset_pk)
        # user_obj = AssetIssue.objects.get(id = asset_pk)
        return render(request, 'stockapp/issue_asset_software.html', 
        {
            'form': SoftwareAssetForm(),
            'pageTitle': 'Issue Asset Form (Fixed Assets)',
            'item_name': asset_name_obj.item_code,
            'asset_name': asset_name_obj.asset_name,
            'brand': asset_name_obj.brand,
            'serial': asset_name_obj.serial,
            'model': asset_name_obj.model,
            'asset_no': asset_name_obj.asset_no,
            'asset_type': asset_name_obj.asset_type,
            'serial': asset_name_obj.serial,
            # 'user_name': user_obj.user_name,
        })
    else:
        try:
            form = SoftwareAssetForm(request.POST)
            if form.is_valid():
                newdata = form.save(commit=False)

                instance_Asset = Asset.objects.get(id = asset_pk)
                newdata.asset = instance_Asset
                newdata.save()
                return redirect('viewallasset')                
            else:
                return render(request, 'stockapp/issue_asset_software.html', {'form': SoftwareAssetForm(), 'error': form.errors})

        except ValueError as error:
            return render(request, 'stockapp/issue_asset_software.html', {'form': SoftwareAssetForm(), 'error': error})  

def viewallissueasset(request):
    issues = AssetIssue.objects.all()
    return render(request, 'stockapp/all_issueasset.html', {'pageTitle': 'All Issue Asset', 'issues' : issues})

def checkout_asset(request, asset_pk):

    
    try:
        # Update
        asset = Asset.objects.get(id=asset_pk)
        asset.check_in_out = 1
        asset.save()

        # asset = get_object_or_404(Asset, pk=asset_pk)        
        # Insert
        log = LogCheckinout.objects.create(
            asset=asset, 
            log_date=timezone.now(), 
            status=1
        )
        log.save()
        # return redirect('viewallasset')
                
    except Asset.DoesNotExist:
        raise Http404
    
    return redirect('viewallasset')

    # if request.method == 'POST':
    #     asset.check_in_out = 1
    #     asset.save()
    #     return redirect('viewallasset') 
        


        