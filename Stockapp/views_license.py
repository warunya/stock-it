from django.shortcuts import render, redirect , get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from datetime import date
from django.db.models import Avg, Max, Min, Sum

from .models import Licenses,ItemcodeMaster
from .forms import (LicensesCreateForm,LicensesViewForm)

def createlicenses(request):
    if request.method == 'GET':
        return render(request, 'stockapp/createlicenses.html', {'form': LicensesCreateForm(), 'pageTitle': 'Create new Licenses'})
    else:
        try:
            form = LicensesCreateForm(request.POST)
            newdata = form.save(commit=False)
            newdata.request_user = request.user
            newdata.save()
            return redirect('viewalllicenses')
        except ValueError as error:
            return render(request, 'stockapp/createlicenses.html', {'form': LicensesCreateForm(), 'error': error})

def viewalllicenses(request):
    licenses = Licenses.objects.all()
    today = date.today()
    detaillicenses = []

    for item in licenses:    
        # combine
        diff = item.warranty_expiredate - today
        status = diff.days

        detaillicenses.append({
            'id': item.id,
            'software_name': item.software_name,
            'software_categorie': item.software_categorie,
            'product_key': item.product_key,
            'qty': item.qty,
            'software_categorie_type' : item.software_categorie_type,
            'warranty_expiredate': item.warranty_expiredate,
            'warranty_startdate': item.warranty_startdate,
            'status' : status,    
        })

    return render(request, 'stockapp/alllicenses.html', {'pageTitle': 'All Licenses', 'licenses': detaillicenses}) 

@login_required
def viewlicenses(request, licenses_pk):    

    item = get_object_or_404(Licenses, pk=licenses_pk)

    if request.method == 'GET':
        form = LicensesViewForm(instance=item)
        return render(request, 'stockapp/viewlicenses.html', {'pageTitle': 'View Licenses Detail', 'item': item, 'form': form})
    else:
        try:
            form = LicensesViewForm(request.POST, instance=item)
            form.save()
            return redirect('viewalllicenses')
        except ValueError as error:
            return render(request, 'stockapp/viewlicenses.html', 
            {
                'pageTitle': 'View Licenses Detail',
                'item': item,
                'form': form,
                'error': error
            })