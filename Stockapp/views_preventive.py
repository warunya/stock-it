from django.shortcuts import render, redirect , get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from datetime import date
from dateutil.relativedelta import *
from django.db.models import Avg, Max, Min, Sum

from .models import AssetMaintenance, Asset, Location, PreventiveComputerSuccess
from .forms import AssetViewForm, PMComputerForm, PMComputerViewForm

from .utils import calcuate_frequency_pm

def allassetpm(request):
    assets = Asset.objects.filter(pm_date_next__isnull=True)
    
    detailassets = []

    for item in assets:    
        # combine 
        detailassets.append({
            'id': item.id,
            'item_code': item.item_code,
            'asset_name': item.asset_name,
            'brand': item.brand,
            'model': item.model,
            'serial' : item.serial,
            'location': item.location,
            'warranty_startdate': item.warranty_startdate,
            'warranty_expiredate': item.warranty_expiredate,
            'type_location_value': item.type_location_value,
            'pm_date_next': item.pm_date_next,
            
        })

    return render(request, 'stockapp/setting_pm_asset.html', {'pageTitle': 'All Asset PM', 'assets': detailassets}) 



def viewassetdetail(request, asset_pk):
    item = get_object_or_404(Asset, pk=asset_pk)

    if request.method == 'GET':
        form = AssetViewForm(instance=item)
        return render(request, 'stockapp/viewasset.html',
        {
            'pageTitle': 'View Asset Detail',
            'item': item,
            'form': form
        })
    else:
        try:
            form = AssetViewForm(request.POST, instance=item)

            newdata = form.save(commit=False)

            # pm_obj = Asset.objects.get(id = 9)
            # old_pm_date_next = pm_obj.pm_date_next
            # old_frequency_pm = pm_obj.frequency_pm
            # new_pm_date_next = pm_obj.pm_date_next + pm_obj.frequency_pm( )

            #==== Calculate Grade move to function
            newdata.frequency_pm = calcuate_frequency_pm(newdata.importance_score, newdata.servicelife_score, newdata.environment_score)          
            newdata.save()
            
            return redirect('allassetpm')
        except ValueError as error:
            return render(request, 'stockapp/viewasset.html',
            {
                'pageTitle': 'View Asset Detail',
                'item': item, 
                'form': form, 
                'error': error
            })

def allassetpmatdate(request):
    assets = Asset.objects.exclude(pm_date_next=None)
    
    detailassets = []

    for item in assets:    
        # combine 
        detailassets.append({
            'id': item.id,
            'item_code': item.item_code,
            'asset_name': item.asset_name,
            'brand': item.brand,
            'model': item.model,
            'serial' : item.serial,
            'location': item.location,
            'warranty_startdate': item.warranty_startdate,
            'warranty_expiredate': item.warranty_expiredate,
            'type_location_value': item.type_location_value,
            'pm_date_next': item.pm_date_next,
            
        })

    return render(request, 'stockapp/all_pm_setting.html', {'pageTitle': 'All Asset PM', 'assets': detailassets})

def pmsuccess(request):
    return render(request, 'stockapp/all_pm_success.html')

def allpmcomputer(request):
    assets = Asset.objects.filter(asset_type=1, pm_date_next__isnull=False)
    
    detailassets = []

    for item in assets:    
        # combine 
        detailassets.append({
            'id': item.id,
            'item_code': item.item_code,
            'asset_name': item.asset_name,
            'brand': item.brand,
            'model': item.model,
            'serial' : item.serial,
            'location': item.location,
            'warranty_startdate': item.warranty_startdate,
            'warranty_expiredate': item.warranty_expiredate,
            'type_location_value': item.type_location_value,
            'pm_date_next': item.pm_date_next,
            
        })
    return render(request, 'stockapp/all_pm_computer.html', {'pageTitle': 'All PM Computer', 'assets': detailassets})

def pmcomputersuccess(request, asset_pk):
    item = get_object_or_404(Asset, pk=asset_pk)

    if request.method == 'GET':
        asset_obj = Asset.objects.get(id = asset_pk)
        return render(request, 'stockapp/pm_computer_success.html', 
        {
            'form': PMComputerForm(),
            'pageTitle': 'Update Computer Preventive Maintenance checklist',
            'asset_no': asset_obj.asset_no

        })
    else:
        try:
            form = PMComputerForm(request.POST)
            if form.is_valid():
                newdata = form.save(commit=False)

                instance_asset = Asset.objects.get(id = asset_pk)
                newdata.asset_id = instance_asset
                newdata.save()

                next_date = newdata.pm_date + relativedelta.relativedelta(months=instance_asset.frequency_pm)
                instance_asset.pm_date_next = next_date
                instance_asset.save()

                return redirect('allpmcomputersuccess')                
            else:
                return render(request, 'stockapp/pm_computer_success.html', {'form': PMComputerForm(), 'error': form.errors})

        except ValueError as error:
            return render(request, 'stockapp/pm_computer_success.html', {'form': PMComputerForm(), 'error': error}) 

def allpmcomputersuccess(request):
    pmcomputers = PreventiveComputerSuccess.objects.all()
    
    detailpmcomputer = []

    for item in pmcomputers:    
        # combine 
        detailpmcomputer.append({
            'id': item.id,
            'asset_id': item.asset_id,
            'pm_date': item.pm_date,
            'pm_by': item.pm_by,
            'hardware_status_value': item.hardware_status_value,
            'comment_value': item.comment_value,
            
        })
    return render(request, 'stockapp/all_pmcomputer_finish.html', {'pageTitle': 'All PM Computer Finish', 'pmcomputers': detailpmcomputer})

def viewpmcomputersuccessdetail(request, pm_pk):
    item = get_object_or_404(PreventiveComputerSuccess, pk=pm_pk)

    if request.method == 'GET':
        form = PMComputerViewForm(instance=item)
        return render(request, 'stockapp/view_pmcomputer_finish.html',
        {
            'pageTitle': 'View Computer Preventive Maintenance checklist',
            'item': item,
            'form': form,
        })
    else:
        try:
            form = PMComputerViewForm(request.POST, instance=item)
            form.save()
            return redirect('allpmcomputersuccess')
        except ValueError as error:
            return render(request, 'stockapp/view_pmcomputer_finish.html',
            {
                'pageTitle': 'View Computer Preventive Maintenance checklist',
                'item': item, 
                'form': form, 
                'error': error
            })

######################################### PM Notebook ##############################################
def allpmlaptop(request):
    assets = Asset.objects.filter(asset_type=2, pm_date_next__isnull=False)
    
    detailassets = []

    for item in assets:    
        # combine 
        detailassets.append({
            'id': item.id,
            'item_code': item.item_code,
            'asset_name': item.asset_name,
            'brand': item.brand,
            'model': item.model,
            'serial' : item.serial,
            'location': item.location,
            'warranty_startdate': item.warranty_startdate,
            'warranty_expiredate': item.warranty_expiredate,
            'type_location_value': item.type_location_value,
            'pm_date_next': item.pm_date_next,
            
        })
    return render(request, 'stockapp/all_pm_computer.html', {'pageTitle': 'All PM Laptop Computer', 'assets': detailassets})


########################################## PM Printer ###############################################
def allpmprinter(request):
    assets = Asset.objects.filter(asset_type=3, pm_date_next__isnull=False)
    
    detailassets = []

    for item in assets:    
        # combine 
        detailassets.append({
            'id': item.id,
            'item_code': item.item_code,
            'asset_name': item.asset_name,
            'brand': item.brand,
            'model': item.model,
            'serial' : item.serial,
            'location': item.location,
            'warranty_startdate': item.warranty_startdate,
            'warranty_expiredate': item.warranty_expiredate,
            'type_location_value': item.type_location_value,
            'pm_date_next': item.pm_date_next,
            
        })
    return render(request, 'stockapp/all_pm_computer.html', {'pageTitle': 'All PM Laptop Computer', 'assets': detailassets})

def allplanpmbymonth(request):
    today = date.today()
    if request.method == 'GET':
        assets = Asset.objects.filter(pm_date_next__isnull=False)
        detailassets = []
        for item in assets:

            date_diff = (item.pm_date_next - today).days

            if date_diff < 0:
                status = 'เลยวันPM'
            elif date_diff == 0 :
                status = 'ถึงวันPM'
            elif date_diff <= 14 :
                status = 'ใกล้ถึงวันPM'
            else:
                status = '-'

            detailassets.append({
                'type_location_value': item.type_location_value,
                'item_code': item.item_code,
                'asset_name': item.asset_name,
                'brand': item.brand,
                'model': item.model,
                'serial': item.serial,
                'location': item.location,
                'warranty_startdate': item.warranty_startdate,
                'warranty_expiredate': item.warranty_expiredate,
                'pm_date_next': item.pm_date_next,
                'date_diff': date_diff,
                'status': status,
                

            })
        return render(request, 'stockapp/all_pm_planbymonth.html', 
        {
            'pageTitle': 'All Plan PM',
            'assets': detailassets
        })  
    elif request.method == "POST":
        start_date = request.POST.get("startdate")
        end_date = request.POST.get("enddate")
        assets = Asset.objects.filter(pm_date_next__range=(start_date, end_date))
        detailassets = []
        for item in assets:

            date_diff = (item.pm_date_next - today).days

            if date_diff < 0:
                status = 'เลยวันPM'
            elif date_diff == 0 :
                status = 'ถึงวันPM'
            elif date_diff <= 14 :
                status = 'ใกล้ถึงวันPM'
            else:
                status = '-'

            detailassets.append({
                'id': item.id,
                'type_location_value': item.type_location_value,
                'item_code': item.item_code,
                'asset_name': item.asset_name,
                'brand': item.brand,
                'model': item.model,
                'serial': item.serial,
                'location': item.location,
                'warranty_startdate': item.warranty_startdate,
                'warranty_expiredate': item.warranty_expiredate,
                'pm_date_next': item.pm_date_next,
                'date_diff': date_diff,
                'status': status,
            })
        return render(request, 'stockapp/all_pm_planbymonth.html',
        {
            'pageTitle': 'All Plan PM By %s To %s' %(start_date,end_date),
            'assets': detailassets,
            'current_startdate': start_date,
            'current_enddate': end_date,
        })

    