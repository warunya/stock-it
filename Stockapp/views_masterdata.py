from django.shortcuts import render, redirect , get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.db.models import Avg, Max, Min, Sum

from django.db.models import Q # Query OR

from .models import People,Department,ItemcodeMaster,List_Of_Software,Manufacturer,Supplier,Location
from .forms_masterdata import (MasterUserCreateForm, MasterUserViewForm, MasterDepartmentCreateForm, MasterDepartmentViewForm,
MasterItemCodeCreateForm, MasterItemCodeViewForm, MasterSoftwareCreateForm, MasterSoftwareViewForm,
MasterManufacturerCreateForm, MasterManufacturerViewForm, MasterSupplierCreateForm, MasterSupplierViewForm,
MasterLocationCreateForm, MasterLocationViewForm)

############## User ########################
def viewalluser(request):
    users = People.objects.all()
    detailusers = []

    for item in users:    
        # combine
        detailusers.append({
            'id': item.id,
            'username': item.username,   
            'first_name': item.first_name,
            'last_name': item.last_name,
            'position': item.position,
            'department': item.department,
            'division_choices_value': item.division_choices_value,
            'email' : item.email,
            'status' : item.status,       
        })
    return render(request, 'stockapp/master_all_user.html', {'pageTitle': 'All User (Master Data)', 'users': users})

def createuser(request):
    if request.method == 'GET':
        return render(request, 'stockapp/master_create_user.html',
        {            
            'form': MasterUserCreateForm(),
            'pageTitle': 'Create new User'
        })
    else:
        try:
            form = MasterUserCreateForm(request.POST, request.FILES)
            if form.is_valid():
                newdata = form.save(commit=False)
                newdata.request_user = request.user
                newdata.created_at = timezone.now()
                newdata.save()
                return redirect('viewalluser')                
            else:
                return render(request, 'stockapp/master_create_user.html',
                {
                    'form': MasterUserCreateForm(),
                    'error': form.errors
                })

        except ValueError as error:
            return render(request, 'stockapp/master_create_user.html', {'form': MasterUserCreateForm(), 'error': error}) 

def viewuserdetail(request, user_pk):
    item = get_object_or_404(People, pk=user_pk)

    if request.method == 'GET':
        form = MasterUserViewForm(instance=item)
        return render(request, 'stockapp/master_view_user.html',
        {
            'pageTitle': 'View User Detail',
            'item': item,
            'form': form
        })
    else:
        try:
            form = MasterUserViewForm(request.POST, instance=item)
            form.save()
            return redirect('viewalluser')
        except ValueError as error:
            return render(request, 'stockapp/master_view_user.html',
            {
                'pageTitle': 'View User Detail',
                'item': item, 
                'form': form, 
                'error': error
            })


######### Department ##############
def viewalldepartment(request):
    departments = Department.objects.all()
    return render(request, 'stockapp/master_all_department.html', {'pageTitle': 'All Department (Master Data)', 'departments': departments})

def createdepartment(request):
    if request.method == 'GET':
        return render(request, 'stockapp/master_create_department.html',
        {            
            'form': MasterDepartmentCreateForm(),
            'pageTitle': 'Create new Department'
        })
    else:
        try:
            form = MasterDepartmentCreateForm(request.POST, request.FILES)
            if form.is_valid():
                newdata = form.save(commit=False)
                newdata.request_user = request.user
                newdata.save()
                return redirect('viewalldepartment')                
            else:
                return render(request, 'stockapp/master_create_department.html',
                {
                    'form': MasterDepartmentCreateForm(),
                    'error': form.errors
                })

        except ValueError as error:
            return render(request, 'stockapp/master_create_department.html', {'form': MasterDepartmentCreateForm(), 'error': error}) 

def viewdepartmentdetail(request, department_pk):
    item = get_object_or_404(Department, pk=department_pk)

    if request.method == 'GET':
        form = MasterDepartmentViewForm(instance=item)
        return render(request, 'stockapp/master_view_department.html',
        {
            'pageTitle': 'View Department Detail',
            'item': item,
            'form': form
        })
    else:
        try:
            form = MasterDepartmentViewForm(request.POST, instance=item)
            form.save()
            return redirect('viewalldepartment')
        except ValueError as error:
            return render(request, 'stockapp/master_view_department.html',
            {
                'pageTitle': 'View Department Detail',
                'item': item, 
                'form': form, 
                'error': error
            })

############## Item Code ###################
def viewallitemcode(request):
    itemcodes = ItemcodeMaster.objects.all()
    detailitems = []

    for item in itemcodes:    
        # combine
        detailitems.append({
            'id': item.id,
            'item_code': item.item_code,   
            'item_name': item.item_name,
            'category_choices_value': item.category_choices_value,  
        })
    return render(request, 'stockapp/master_all_itemcode.html', {'pageTitle': 'All ItemCode (Master Data)', 'itemcodes': detailitems})

def createitemcode(request):
    if request.method == 'GET':
        return render(request, 'stockapp/master_create_itemcode.html',
        {            
            'form': MasterItemCodeCreateForm(),
            'pageTitle': 'Create new ItemCode'
        })
    else:
        try:
            form = MasterItemCodeCreateForm(request.POST, request.FILES)
            if form.is_valid():
                newdata = form.save(commit=False)
                newdata.request_user = request.user
                newdata.save()
                return redirect('viewallitemcode')                
            else:
                return render(request, 'stockapp/master_create_itemcode.html',
                {
                    'form': MasterItemCodeCreateForm(),
                    'error': form.errors
                })

        except ValueError as error:
            return render(request, 'stockapp/master_create_itemcode.html', {'form': MasterItemCodeCreateForm(), 'error': error}) 

def viewitemcodedetail(request, itemcode_pk):
    item = get_object_or_404(ItemcodeMaster, pk=itemcode_pk)

    if request.method == 'GET':
        form = MasterItemCodeViewForm(instance=item)
        return render(request, 'stockapp/master_view_itemcode.html',
        {
            'pageTitle': 'View ItemCode Detail',
            'item': item,
            'form': form
        })
    else:
        try:
            form = MasterItemCodeViewForm(request.POST, instance=item)
            form.save()
            return redirect('viewallitemcode')
        except ValueError as error:
            return render(request, 'stockapp/master_view_itemcode.html',
            {
                'pageTitle': 'View ItemCode Detail',
                'item': item, 
                'form': form, 
                'error': error
            })

############# Software List #########
def viewallsoftware(request):
    softwares = List_Of_Software.objects.all()
    return render(request, 'stockapp/master_all_software.html', {'pageTitle': 'All Software (Master Data)', 'softwares': softwares})

def createsoftware(request):
    if request.method == 'GET':
        return render(request, 'stockapp/master_create_software.html',
        {            
            'form': MasterSoftwareCreateForm(),
            'pageTitle': 'Create new Softtware'
        })
    else:
        try:
            form = MasterSoftwareCreateForm(request.POST, request.FILES)
            if form.is_valid():
                newdata = form.save(commit=False)
                newdata.request_user = request.user
                newdata.save()
                return redirect('viewallsoftware')                
            else:
                return render(request, 'stockapp/master_create_software.html',
                {
                    'form': MasterSoftwareCreateForm(),
                    'error': form.errors
                })

        except ValueError as error:
            return render(request, 'stockapp/master_create_software.html', {'form': MasterSoftwareCreateForm(), 'error': error}) 

def viewsoftwaredetail(request, software_pk):
    item = get_object_or_404(List_Of_Software, pk=software_pk)

    if request.method == 'GET':
        form = MasterSoftwareViewForm(instance=item)
        return render(request, 'stockapp/master_view_software.html',
        {
            'pageTitle': 'View Software Detail',
            'item': item,
            'form': form
        })
    else:
        try:
            form = MasterSoftwareViewForm(request.POST, instance=item)
            form.save()
            return redirect('viewallsoftware')
        except ValueError as error:
            return render(request, 'stockapp/master_view_software.html',
            {
                'pageTitle': 'View Software Detail',
                'item': item, 
                'form': form, 
                'error': error
            })

############## Manufacturer #####################
def viewallmanufacturer(request):
    manufacturers = Manufacturer.objects.all()
    return render(request, 'stockapp/master_all_manufacturer.html', {'pageTitle': 'All Manufacturer (Master Data)', 'manufacturers': manufacturers})

def createmanufacturer(request):
    if request.method == 'GET':
        return render(request, 'stockapp/master_create_manufacturer.html',
        {            
            'form': MasterManufacturerCreateForm(),
            'pageTitle': 'Create new Manufacturer'
        })
    else:
        try:
            form = MasterManufacturerCreateForm(request.POST, request.FILES)
            if form.is_valid():
                newdata = form.save(commit=False)
                newdata.request_user = request.user
                newdata.save()
                return redirect('viewallmanufacturer')                
            else:
                return render(request, 'stockapp/master_create_manufacturer.html',
                {
                    'form': MasterManufacturerCreateForm(),
                    'error': form.errors
                })

        except ValueError as error:
            return render(request, 'stockapp/master_create_manufacturer.html', {'form': MasterManufacturerCreateForm(), 'error': error}) 

def viewmanufacturerdetail(request, manufacturer_pk):
    item = get_object_or_404(Manufacturer, pk=manufacturer_pk)

    if request.method == 'GET':
        form = MasterManufacturerViewForm(instance=item)
        return render(request, 'stockapp/master_view_manufacturer.html',
        {
            'pageTitle': 'View Manufacturer Detail',
            'item': item,
            'form': form
        })
    else:
        try:
            form = MasterManufacturerViewForm(request.POST, instance=item)
            form.save()
            return redirect('viewallmanufacturer')
        except ValueError as error:
            return render(request, 'stockapp/master_view_manufacturer.html',
            {
                'pageTitle': 'View Manufacturer Detail',
                'item': item, 
                'form': form, 
                'error': error
            })

################## Supplier #######################
def viewallsupplier(request):
    suppliers = Supplier.objects.all()
    detailitems = []

    for item in suppliers:    
        # combine
        detailitems.append({
            'id': item.id,
            'supplier_name': item.supplier_name,   
            'mobile_phone': item.mobile_phone,
            'office_phone': item.office_phone,
            'fax': item.fax,
            'email': item.email,
            'termofpayment_choices_value': item.termofpayment_choices_value,  
        })
    return render(request, 'stockapp/master_all_supplier.html', {'pageTitle': 'All Suplier (Master Data)', 'suppliers': detailitems})

def createsupplier(request):
    if request.method == 'GET':
        return render(request, 'stockapp/master_create_supplier.html',
        {            
            'form': MasterSupplierCreateForm(),
            'pageTitle': 'Create new Supplier'
        })
    else:
        try:
            form = MasterSupplierCreateForm(request.POST, request.FILES)
            if form.is_valid():
                newdata = form.save(commit=False)
                newdata.request_user = request.user
                newdata.save()
                return redirect('viewallsupplier')                
            else:
                return render(request, 'stockapp/master_create_supplier.html',
                {
                    'form': MasterSupplierCreateForm(),
                    'error': form.errors
                })

        except ValueError as error:
            return render(request, 'stockapp/master_create_supplier.html', {'form': MasterSupplierCreateForm(), 'error': error}) 

def viewsupplierdetail(request, supplier_pk):
    item = get_object_or_404(Supplier, pk=supplier_pk)

    if request.method == 'GET':
        form = MasterSupplierViewForm(instance=item)
        return render(request, 'stockapp/master_view_supplier.html',
        {
            'pageTitle': 'View Supplier Detail',
            'item': item,
            'form': form
        })
    else:
        try:
            form = MasterSupplierViewForm(request.POST, instance=item)
            form.save()
            return redirect('viewallsupplier')
        except ValueError as error:
            return render(request, 'stockapp/master_view_supplier.html',
            {
                'pageTitle': 'View Supplier Detail',
                'item': item, 
                'form': form, 
                'error': error
            })

##################### Location #########################
def viewalllocation(request):
    locations = Location.objects.all()
    return render(request, 'stockapp/master_all_location.html', {'pageTitle': 'All Location (Master Data)', 'locations': locations})

def createlocation(request):
    if request.method == 'GET':
        return render(request, 'stockapp/master_create_location.html',
        {            
            'form': MasterLocationCreateForm(),
            'pageTitle': 'Create new Location'
        })
    else:
        try:
            form = MasterLocationCreateForm(request.POST, request.FILES)
            if form.is_valid():
                newdata = form.save(commit=False)
                newdata.request_user = request.user
                newdata.save()
                return redirect('viewalllocation')                
            else:
                return render(request, 'stockapp/master_create_location.html',
                {
                    'form': MasterLocationCreateForm(),
                    'error': form.errors
                })

        except ValueError as error:
            return render(request, 'stockapp/master_create_location.html', {'form': MasterLocationCreateForm(), 'error': error}) 

def viewlocationdetail(request, location_pk):
    item = get_object_or_404(Location, pk=location_pk)

    if request.method == 'GET':
        form = MasterLocationViewForm(instance=item)
        return render(request, 'stockapp/master_view_location.html',
        {
            'pageTitle': 'View Location Detail',
            'item': item,
            'form': form
        })
    else:
        try:
            form = MasterLocationViewForm(request.POST, instance=item)
            form.save()
            return redirect('viewalllocation')
        except ValueError as error:
            return render(request, 'stockapp/master_view_location.html',
            {
                'pageTitle': 'View Location Detail',
                'item': item, 
                'form': form, 
                'error': error
            })

