from django.shortcuts import render
from django.conf import settings
from django.http import FileResponse, Http404

from .models import People, Asset, PreventiveComputerSuccess, AssetIssue, AssetSoftware, List_Of_Software, SoftwareForAsset

import io
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont

from PyPDF2 import PdfFileWriter, PdfFileReader
from io import BytesIO
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import A4, A3

# Ref https://gist.github.com/kzim44/5023021

# Library Requirements.

# pip install reportlab
# pip install PyPDF2

#### Form User Domain Group
def GeneratePDFUserDomain(request, username):
    if request.method == 'GET':
        buffer = io.BytesIO()
        # create a new PDF with Reportlab
        pdfmetrics.registerFont(TTFont('ARIALUNI', 'ARIALUNI.ttf'))
        p = canvas.Canvas(buffer, pagesize=A4)
        p.setFont("ARIALUNI", 9)

        try:
            user_obj = People.objects.get(username=username)
            name = '%s %s' %(user_obj.first_name, user_obj.last_name)
            email = '%s' %(user_obj.email)
        except People.DoesNotExist as error:
            name = 'find not found'

        p.drawString(178, 680, name)
        p.drawString(178, 657, username)
        p.drawString(178, 429, email)
        p.drawString(178, 452, name)

# # ***** 1 ******
#         for seq in range(1,78):
#             p.drawString(5, seq*10, "x: %d y:%d ทดสอบ" %(5, seq*10) )   
# # ***** 2 ******        
#         for seq in range(1,78):
#             p.drawString(80, seq*10, "x: %d y:%d ทดสอบ" %(80, seq*10) )   
# # ***** 3 ******
#         for seq in range(1,78):
#             p.drawString(160, seq*10, "x: %d y:%d ทดสอบ" %(160, seq*10) )   
# # ***** 4 ******
#         for seq in range(1,78):
#             p.drawString(250, seq*10, "x: %d y:%d ทดสอบ" %(250, seq*10) )   
# # ***** 5 ******
#         for seq in range(1,78):
#             p.drawString(340, seq*10, "x: %d y:%d ทดสอบ" %(340, seq*10) )   
# # ***** 6 ******
#         for seq in range(1,78):
#             p.drawString(430, seq*10, "x: %d y:%d ทดสอบ" %(430, seq*10) )   
# # ***** 7 ******
#         for seq in range(1,78):
#             p.drawString(520, seq*10, "x: %d y:%d ทดสอบ" %(520, seq*10) )   

        p.showPage()
        p.save()

        buffer.seek(0)
        newPdf = PdfFileReader(buffer)
               


        # #######DEBUG NEW PDF created#############
        pdf1 = buffer.getvalue()
        open(settings.MEDIA_ROOT + '/pdf/pdf1.pdf', 'wb').write(pdf1)
        # #########################################

        # read your existing PDF
        if email=='':
            pdf_template = settings.MEDIA_ROOT + '/pdf/template_userdomainnoemail.pdf'
        else:
            pdf_template = settings.MEDIA_ROOT + '/pdf/template_userdomain.pdf'
        existingPdf = PdfFileReader(open(pdf_template, 'rb'))
        output = PdfFileWriter()
        # add the "watermark" (which is the new pdf) on the existing page
        page = existingPdf.getPage(0)
        page.mergePage(newPdf.getPage(0))
        output.addPage(page)
        # finally, write "output" to a real file
        filename = '%s/pdf/%s.pdf'%(settings.MEDIA_ROOT, username)
        outputStream =  open(filename, 'wb')
        output.write(outputStream)
        outputStream.close()

        try:             
            return FileResponse(open(filename, 'rb'), content_type='application/pdf')                
        except FileNotFoundError:
            raise Http404()

def GeneratePDFPMComputer(request, id, asset_id):
    if request.method == 'GET':
        buffer = io.BytesIO()
        # create a new PDF with Reportlab
        pdfmetrics.registerFont(TTFont('ARIALUNI', 'ARIALUNI.ttf'))
        p = canvas.Canvas(buffer, pagesize=A4)
        p.setFont("ARIALUNI", 10)

        try:
            pmasset_obj = PreventiveComputerSuccess.objects.get(id=id)
            pm_date = pmasset_obj.pm_date.strftime("%m/%d/%Y")
            pm_by = '%s' %(pmasset_obj.pm_by)
            comment_pm = '%s' %(pmasset_obj.get_comment_pm_display())
            if pmasset_obj.os == None :
                os = ''
            else :
                os = '%s' %(pmasset_obj.os)

            health_status = '%s' %(pmasset_obj.get_health_status_display())
            if pmasset_obj.remark == None :
                remark = ''
            else :
                remark = '%s' %(pmasset_obj.remark)


        except PreventiveComputerSuccess.DoesNotExist as error:
            pm_date = ''
            pm_by = ''
            comment_pm = ''
            os = ''
            health_status = ''
            remark = ''

        try:
            asset_obj = Asset.objects.get(id=asset_id) # id = Pk , asset_id = Variable
            asset_type= asset_obj.get_asset_type_display()
            brand = '%s' %(asset_obj.brand)
            model = '%s' %(asset_obj.model)
            serial = '%s' %(asset_obj.serial)
            asset_name = '%s' %(asset_obj.asset_name)
            ip_address_1 = '%s' %(asset_obj.ip_address_1)
            location = '%s' %(asset_obj.location)

        except Asset.DoesNotExist as error:
            asset_type = ''
            brand = ''
            model = ''
            serial = ''
            ip_address_1 = ''


        try:
            issue_obj = AssetIssue.objects.get(asset_id_id=asset_id,id=id)
            user_obj = People.objects.get(id=issue_obj.user_id_id)
            username = '%s' %(user_obj.username)
        except AssetIssue.DoesNotExist as error:
            username = 'find not found'


        p.drawString(150, 758, pm_date)
        p.drawString(95, 734, asset_type)
        p.drawString(280, 734, brand)
        p.drawString(430, 734, model)
        p.drawString(140, 707, serial)
        p.drawString(140, 691, asset_name)
        p.drawString(140, 676, ip_address_1)
        p.drawString(140, 659, location)
        p.drawString(90, 220 , comment_pm)
        p.drawString(90, 210, os)
        p.drawString(202, 172, health_status)
        p.drawString(90, 160, remark)
        p.drawString(340, 126, pm_by)
        p.setFont("ARIALUNI", 26)
        p.drawString(355, 660, username)

# # ***** 1 ******
#         for seq in range(3,80):
#             p.drawString(5, seq*10, "x: %d y:%d ทดสอบ" %(5, seq*10) )   
# # ***** 2 ******        
#         for seq in range(3,80):
#             p.drawString(80, seq*10, "x: %d y:%d ทดสอบ" %(80, seq*10) )   
# # ***** 3 ******
#         for seq in range(3,80):
#             p.drawString(160, seq*10, "x: %d y:%d ทดสอบ" %(160, seq*10) )   
# # ***** 4 ******
#         for seq in range(3,80):
#             p.drawString(250, seq*10, "x: %d y:%d ทดสอบ" %(250, seq*10) )   
# # ***** 5 ******
#         for seq in range(3,80):
#             p.drawString(340, seq*10, "x: %d y:%d ทดสอบ" %(340, seq*10) )   
# # ***** 6 ******
#         for seq in range(3,80):
#             p.drawString(430, seq*10, "x: %d y:%d ทดสอบ" %(430, seq*10) )   
# # ***** 7 ******
#         for seq in range(3,80):
#             p.drawString(520, seq*10, "x: %d y:%d ทดสอบ" %(520, seq*10) )   

        p.showPage()
        p.save()

        buffer.seek(0)
        newPdf = PdfFileReader(buffer)
               


        # #######DEBUG NEW PDF created#############
        pdf1 = buffer.getvalue()
        open(settings.MEDIA_ROOT + '/pdf/pdf1.pdf', 'wb').write(pdf1)
        # #########################################

        # read your existing PDF
        pdf_template = settings.MEDIA_ROOT + '/pdf/template_pmcomputer.pdf'
        existingPdf = PdfFileReader(open(pdf_template, 'rb'))
        output = PdfFileWriter()
        # add the "watermark" (which is the new pdf) on the existing page
        page = existingPdf.getPage(0)
        page.mergePage(newPdf.getPage(0))
        output.addPage(page)
        # finally, write "output" to a real file
        filename = '%s/pdf/%s.pdf'%(settings.MEDIA_ROOT, asset_name)
        outputStream =  open(filename, 'wb')
        output.write(outputStream)
        outputStream.close()

        try:             
            return FileResponse(open(filename, 'rb'), content_type='application/pdf')                
        except FileNotFoundError:
            raise Http404()

def GeneratePDFComputerReceiveReturn(request, asset_id, issued_id):
    if request.method == 'GET':
        buffer = io.BytesIO()
        # create a new PDF with Reportlab
        pdfmetrics.registerFont(TTFont('ARIALUNI', 'ARIALUNI.ttf'))
        pdfmetrics.registerFont(TTFont('ARIALNB', 'ARIALNB.ttf'))
        p = canvas.Canvas(buffer, pagesize=A4)
        p.setFont("ARIALUNI", 8)

        try:
            asset_obj = Asset.objects.get(id=asset_id) # id = Pk , asset_id = Variable
            asset_type= asset_obj.get_asset_type_display() 
            brand = '%s' %(asset_obj.brand)
            model = '%s' %(asset_obj.model)
            serial = '%s' %(asset_obj.serial)
            asset_name = '%s' %(asset_obj.asset_name)
            ip_address_1 = '%s' %(asset_obj.ip_address_1)
            location = '%s' %(asset_obj.location)
            if asset_obj.mac_address_lan_1 == None:
                mac_address_lan_1 = ''
            else:
                mac_address_lan_1 = '%s' %(asset_obj.mac_address_lan_1)

            if asset_obj.mac_address_wifi == None:
                mac_address_wifi = ''
            else:
                mac_address_wifi = '%s' %(asset_obj.mac_address_wifi)

        except Asset.DoesNotExist as error:
            asset_type = ''
            brand = ''
            model = ''
            serial = ''
            ip_address_1 = ''
            asset_name = ''
            location = ''
            mac_address_lan_1 = ''
            mac_address_wifi = ''

        try:
            issue_obj = AssetIssue.objects.get(asset_id_id=asset_id,id=issued_id)
            user_obj = People.objects.get(id=issue_obj.user_id_id)
            username = '%s %s' %(user_obj.first_name, user_obj.last_name)
            if user_obj.position == None:
                position = ''
            else:                
                position = '%s' %(user_obj.position)

            department = '%s' %(user_obj.department)
            division = user_obj.get_division_display()

        except AssetIssue.DoesNotExist as error:
            username = 'find not found'
            position = ''
            department = ''
            division = ''

        try:
            software_obj = SoftwareForAsset.objects.get(asset_id=asset_id)
            if software_obj.os_name == True :
                os_name = '/'
            else:
                os_name = 'X'
            
            if software_obj.office == True :
                office = '/'
            else:
                office = 'X'
            
            if software_obj.browser == True:
                browser = '/'
            else:
                browser = 'X'

            if software_obj.email == True:
                email = '/'
            else:
                email = 'X'
                
            if software_obj.helpdesk == True:
                helpdesk = '/'
            else:
                helpdesk = 'X'
            
            if software_obj.lineapp == True:
                lineapp = '/'
            else:
                lineapp = 'X'

            if software_obj.antivirus == True:
                antivirus = '/'
            else:
                antivirus = 'X'

            if software_obj.mta == True:
                mta = '/'
            else:
                mta = 'X'

            if software_obj.ax == True:
                ax = '/'
            else:
                ax = 'X'

            if software_obj.special_check1 == True:
                special_check1 = '/'
            else:
                special_check1 = ''

            if software_obj.special_check2 == True:
                special_check2 = '/'
            else:
                special_check2 = ''

            if software_obj.special_check3 == True:
                special_check3 = '/'
            else:
                special_check3 = ''

            if software_obj.special_check4 == True:
                special_check4 = '/'
            else:
                special_check4 = ''

            if software_obj.special_check5 == True:
                special_check5 = '/'
            else:
                special_check5 = ''

            if software_obj.mouse == True:
                mouse = '/'
            else:
                mouse = 'X'

            if software_obj.keyboard == True:
                keyboard = '/'
            else:
                keyboard = 'X'

            if software_obj.adaptor == True:
                adaptor = '/'
            else:
                adaptor = 'X'

            if software_obj.gigbag == True:
                gigbag = '/'
            else:
                gigbag = 'X'

            
            os_std = '%s' %(software_obj.os_std)
            office_std = '%s' %(software_obj.office_std)
            browser_std = '%s' %(software_obj.browser_std)
            email_std = '%s' %(software_obj.email_std)
            helpdesk_std = '%s' %(software_obj.helpdesk_std)
            line_std = '%s' %(software_obj.line_std)
            antivirus_std = '%s' %(software_obj.antivirus_std)
            software_Special1 = '%s' %(software_obj.software_Special1)
            program_name1 = '%s' %(software_obj.program_name1)
            software_Special2 = '%s' %(software_obj.software_Special2)
            program_name2 = '%s' %(software_obj.program_name2)
            software_Special3 = '%s' %(software_obj.software_Special3)
            program_name3 = '%s' %(software_obj.program_name3)
            software_Special4 = '%s' %(software_obj.software_Special4)
            program_name4 = '%s' %(software_obj.program_name4)
            software_Special5 = '%s' %(software_obj.software_Special5)
            program_name5 = '%s' %(software_obj.program_name5)


        except SoftwareForAsset.DoesNotExist as error:
            os_name = ''   
            special_check1 = ''
            special_check2 = ''
            special_check3 = ''
            special_check3 = ''
            special_check4 = ''
            special_check5 = ''

        p.drawString(120, 690, asset_type)
        p.drawString(120, 679.5, brand)
        p.drawString(120, 670, model)
        p.drawString(120, 659, serial)
        p.drawString(355, 690, asset_name)
        p.drawString(355, 679, mac_address_lan_1)
        p.drawString(355, 670, mac_address_wifi)
        p.drawString(355, 659, ip_address_1)
        p.drawString(95, 638, username)
        p.drawString(95, 628, position)
        p.drawString(95, 618, department)
        p.drawString(95, 607.5, division)
        p.drawString(370,576.5, os_std)
        p.drawString(80,437,software_Special1)
        p.drawString(80,427,software_Special2)
        p.drawString(80,416,software_Special3)
        p.drawString(80,405,software_Special4)
        p.drawString(80,394,software_Special5)
        p.drawString(372,437,program_name1)
        p.drawString(372,427,program_name2)
        p.drawString(372,416,program_name3)
        p.drawString(372,405,program_name4)
        p.drawString(372,394,program_name5)
        p.drawString(370,555,office_std)
        p.drawString(370,544.5,browser_std)
        p.drawString(370,533,email_std)
        p.drawString(370,523,helpdesk_std)
        p.drawString(370,512,line_std)
        p.drawString(370,501,antivirus_std)


        p.setFont("ARIALNB", 11)
        p.drawString(340, 575, os_name)
        p.drawString(340, 554, office)
        p.drawString(340, 543, browser)
        p.drawString(340, 532, email)
        p.drawString(340, 522, helpdesk)
        p.drawString(340, 511, lineapp)
        p.drawString(340, 499.5, antivirus)
        p.drawString(340, 478, mta)
        p.drawString(338, 467, ax)
        p.drawString(340, 436, special_check1)
        p.drawString(340, 424, special_check2)
        p.drawString(340, 414, special_check3)
        p.drawString(340, 403.5, special_check4)
        p.drawString(340, 393, special_check5)

        p.drawString(295, 340, mouse)
        p.drawString(295, 329, keyboard)
        p.drawString(295, 319, adaptor)
        p.drawString(295, 308, gigbag)




# # ***** 1 ******
#         for seq in range(3,80):
#             p.drawString(5, seq*10, "x: %d y:%d ทดสอบ" %(5, seq*10) )   
# # ***** 2 ******        
#         for seq in range(3,80):
#             p.drawString(80, seq*10, "x: %d y:%d ทดสอบ" %(80, seq*10) )   
# # ***** 3 ******
#         for seq in range(3,80):
#             p.drawString(160, seq*10, "x: %d y:%d ทดสอบ" %(160, seq*10) )   
# # ***** 4 ******
#         for seq in range(3,80):
#             p.drawString(250, seq*10, "x: %d y:%d ทดสอบ" %(250, seq*10) )   
# # ***** 5 ******
#         for seq in range(3,80):
#             p.drawString(340, seq*10, "x: %d y:%d ทดสอบ" %(340, seq*10) )   
# # ***** 6 ******
#         for seq in range(3,80):
#             p.drawString(430, seq*10, "x: %d y:%d ทดสอบ" %(430, seq*10) )   
# # ***** 7 ******
#         for seq in range(3,80):
#             p.drawString(520, seq*10, "x: %d y:%d ทดสอบ" %(520, seq*10) )   

        p.showPage()
        p.save()

        buffer.seek(0)
        newPdf = PdfFileReader(buffer)
               


        # #######DEBUG NEW PDF created#############
        pdf1 = buffer.getvalue()
        open(settings.MEDIA_ROOT + '/pdf/pdf1.pdf', 'wb').write(pdf1)
        # #########################################

        # read your existing PDF
        pdf_template = settings.MEDIA_ROOT + '/pdf/template_computer_Receive_Return.pdf'
        existingPdf = PdfFileReader(open(pdf_template, 'rb'))
        output = PdfFileWriter()
        # add the "watermark" (which is the new pdf) on the existing page
        page = existingPdf.getPage(0)
        page.mergePage(newPdf.getPage(0))
        output.addPage(page)
        # finally, write "output" to a real file
        filename = '%s/pdf/%s.pdf'%(settings.MEDIA_ROOT, serial)
        outputStream =  open(filename, 'wb')
        output.write(outputStream)
        outputStream.close()
 
        try:             
            return FileResponse(open(filename, 'rb'), content_type='application/pdf')                
        except FileNotFoundError:
            raise Http404()

