from django.forms import ModelForm
from .models import (Licenses, Consumable, Accessorie, Component, AssetMaintenance,Asset, AccessoriesIssue,
 ConsumablesIssue, ComponentsIssue,AssetIssue,AssetMaintenance,Leases, PreventiveComputerSuccess, SoftwareForAsset)
from django import forms

class DateInput(forms.DateInput):
    input_type = 'date'

class LicensesCreateForm(ModelForm):
    class Meta:
        model = Licenses
        fields = ['software_name', 'software_categorie','software_version','product_key','qty','manufacturer','licensed_to_name',
                  'licensed_to_email', 'suppiler', 'purchase_cost', 'invoice_number', 'receive_date', 'warranty_startdate', 'warranty_expiredate',
                  'depreciation', 'notes']
        widgets = {
            'software_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'software_categorie' : forms.Select(attrs={'class': 'form-control'}),
            'software_version' : forms.TextInput(attrs={'class': 'form-control'}),
            'product_key' : forms.TextInput(attrs={'class': 'form-control'}),
            'qty' : forms.NumberInput(attrs={'class': 'form-control'}),
            'manufacturer' : forms.Select(attrs={'class': 'form-control'}),
            'licensed_to_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'licensed_to_email' : forms.TextInput(attrs={'class': 'form-control'}),
            'suppiler' : forms.Select(attrs={'class': 'form-control'}),
            'purchase_cost' : forms.NumberInput(attrs={'class': 'form-control'}),
            'invoice_number' : forms.TextInput(attrs={'class': 'form-control'}),
            'receive_date' : DateInput(attrs={'class': 'form-control'}),
            'warranty_startdate' : DateInput(attrs={'class': 'form-control'}),
            'warranty_expiredate' : DateInput(attrs={'class': 'form-control'}),
            'depreciation' : forms.Select(attrs={'class': 'form-control'}),
            'notes' : forms.Textarea(attrs={'class': 'form-control'}),
        }

class LicensesViewForm(ModelForm):
    class Meta:
        model = Licenses
        fields = ['software_name', 'software_categorie', 'software_version', 'product_key','qty','manufacturer','licensed_to_name',
                  'licensed_to_email', 'suppiler', 'purchase_cost', 'invoice_number', 'receive_date', 'warranty_startdate', 'warranty_expiredate',
                  'depreciation', 'notes']
        widgets = {
            'software_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'software_categorie' : forms.Select(attrs={'class': 'form-control'}),
            'software_version' : forms.TextInput(attrs={'class': 'form-control'}),
            'product_key' : forms.TextInput(attrs={'class': 'form-control', 'readonly':True}),
            'qty' : forms.NumberInput(attrs={'class': 'form-control'}),
            'manufacturer' : forms.Select(attrs={'class': 'form-control'}),
            'licensed_to_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'licensed_to_email' : forms.TextInput(attrs={'class': 'form-control'}),
            'suppiler' : forms.Select(attrs={'class': 'form-control'}),
            'purchase_cost' : forms.NumberInput(attrs={'class': 'form-control'}),
            'invoice_number' : forms.TextInput(attrs={'class': 'form-control' , 'readonly':True}),
            'receive_date' : DateInput(attrs={'class': 'form-control'}),
            'warranty_startdate' : DateInput(attrs={'class': 'form-control'}),
            'warranty_expiredate' : DateInput(attrs={'class': 'form-control'}),
            'depreciation' : forms.Select(attrs={'class': 'form-control'}),
            'notes' : forms.Textarea(attrs={'class': 'form-control'}),
        }

class ConsumableCreateForm(ModelForm):
    def clean_image(self):
        file = self.cleaned_data.get("image", False)
        if file:
            ext = file.name.split('.')[-1].lower()
            if ext not in ["jpg", "jpeg", "png"]:
                raise forms.ValidationError("อนุญาตเฉพาะไฟล์ jpg, jpeg และ png เท่านั้น")

        return file
    class Meta:
        model = Consumable
        fields = ['consumable_name','consumable_catagory','manufacturer', 'location','brand',
                  'model','serial','suppiler','purchase_cost','invoice_number','receive_date','warranty_startdate',
                  'warranty_expiredate','qty','min_qty','image']
        widgets = {
            'consumable_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'consumable_catagory' : forms.Select(attrs={'class': 'form-control'}),
            'manufacturer' : forms.Select(attrs={'class': 'form-control'}),
            'location' : forms.Select(attrs={'class': 'form-control'}),
            'brand' : forms.Select(attrs={'class': 'form-control'}),
            'model' : forms.TextInput(attrs={'class': 'form-control'}),
            'serial' : forms.TextInput(attrs={'class': 'form-control'}),
            'suppiler' : forms.Select(attrs={'class': 'form-control'}),
            'purchase_cost' : forms.NumberInput(attrs={'class': 'form-control'}),
            'invoice_number' : forms.TextInput(attrs={'class': 'form-control'}),
            'receive_date' : DateInput(attrs={'class': 'form-control'}),
            'warranty_startdate' : DateInput(attrs={'class': 'form-control'}),
            'warranty_expiredate' : DateInput(attrs={'class': 'form-control'}),
            'qty' : forms.NumberInput(attrs={'class': 'form-control'}),
            'min_qty' : forms.NumberInput(attrs={'class': 'form-control'}),
            'image' : forms.ClearableFileInput(attrs={'class': 'form-control'}),

        }

class ConsumableViewForm(ModelForm):
    class Meta:
        model = Consumable
        fields = ['itemcode','consumable_name','consumable_catagory','manufacturer', 'location','brand',
                  'model','serial','suppiler','purchase_cost','invoice_number','receive_date','warranty_startdate',
                  'warranty_expiredate','qty','min_qty']
        widgets = {
            'itemcode' : forms.Select(attrs={'class': 'form-control', 'readonly':True}),
            'consumable_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'consumable_catagory' : forms.Select(attrs={'class': 'form-control'}),
            'manufacturer' : forms.Select(attrs={'class': 'form-control'}),
            'location' : forms.Select(attrs={'class': 'form-control'}),
            'brand' : forms.TextInput(attrs={'class': 'form-control'}),
            'model' : forms.TextInput(attrs={'class': 'form-control'}),
            'serial' : forms.TextInput(attrs={'class': 'form-control', 'readonly':True}),
            'suppiler' : forms.Select(attrs={'class': 'form-control'}),
            'purchase_cost' : forms.NumberInput(attrs={'class': 'form-control'}),
            'invoice_number' : forms.TextInput(attrs={'class': 'form-control'}),
            'receive_date' : DateInput(attrs={'class': 'form-control'}),
            'warranty_startdate' : DateInput(attrs={'class': 'form-control'}),
            'warranty_expiredate' : DateInput(attrs={'class': 'form-control'}),
            'qty' : forms.NumberInput(attrs={'class': 'form-control'}),
            'min_qty' : forms.NumberInput(attrs={'class': 'form-control'}),

        }

class ConsumableIssueForm(ModelForm):
    class Meta:
        model = ConsumablesIssue
        fields = ['department_id','assigned_qty']
        widgets = {           
            'department_id' : forms.Select(attrs={'class': 'form-control'}),
            'assigned_qty' : forms.TextInput(attrs={'class': 'form-control'}),
               
        }

class AccessorieCreateForm(ModelForm):
    def clean_image(self):
        file = self.cleaned_data.get("image", False)
        if file:
            ext = file.name.split('.')[-1].lower()
            if ext not in ["jpg", "jpeg", "png"]:
                raise forms.ValidationError("อนุญาตเฉพาะไฟล์ jpg, jpeg และ png เท่านั้น")

        return file
    class Meta:
        model = Accessorie
        fields = ['itemcode','accessorie_name', 'manufacturer', 'location', 'brand', 'model' , 'serial', 'suppiler',
                  'purchase_cost', 'invoice_number', 'receive_date', 'warranty_startdate', 'warranty_expiredate',
                  'qty', 'min_qty', 'image']
        widgets = {
            'itemcode' : forms.Select(attrs={'class': 'form-control'}),
            'accessorie_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'consumable_catagory' : forms.Select(attrs={'class': 'form-control'}),
            'manufacturer' : forms.Select(attrs={'class': 'form-control'}),
            'location' : forms.Select(attrs={'class': 'form-control'}),
            'brand' : forms.Select(attrs={'class': 'form-control'}),
            'model' : forms.TextInput(attrs={'class': 'form-control'}),
            'serial' : forms.TextInput(attrs={'class': 'form-control'}),
            'suppiler' : forms.Select(attrs={'class': 'form-control'}),
            'purchase_cost' : forms.NumberInput(attrs={'class': 'form-control'}),
            'invoice_number' : forms.TextInput(attrs={'class': 'form-control'}),
            'receive_date' : DateInput(attrs={'class': 'form-control'}),
            'warranty_startdate' : DateInput(attrs={'class': 'form-control'}),
            'warranty_expiredate' : DateInput(attrs={'class': 'form-control'}),
            'qty' : forms.NumberInput(attrs={'class': 'form-control'}),
            'min_qty' : forms.NumberInput(attrs={'class': 'form-control'}),
            'image' : forms.ClearableFileInput(attrs={'class': 'form-control'}),

        }

class AccessorieViewForm(ModelForm):
    class Meta:
        model = Accessorie
        fields = ['itemcode','accessorie_name', 'manufacturer', 'location', 'brand', 'model' , 'serial', 'suppiler',
                  'purchase_cost', 'invoice_number', 'receive_date', 'warranty_startdate', 'warranty_expiredate',
                  'qty', 'min_qty']
        widgets = {
            'itemcode' : forms.Select(attrs={'class': 'form-control', 'readonly':True}),
            'accessorie_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'manufacturer' : forms.Select(attrs={'class': 'form-control'}),
            'location' : forms.Select(attrs={'class': 'form-control'}),
            'brand' : forms.Select(attrs={'class': 'form-control'}),
            'model' : forms.TextInput(attrs={'class': 'form-control'}),
            'serial' : forms.TextInput(attrs={'class': 'form-control'}),
            'suppiler' : forms.Select(attrs={'class': 'form-control'}),
            'purchase_cost' : forms.NumberInput(attrs={'class': 'form-control', 'readonly':True}),
            'invoice_number' : forms.TextInput(attrs={'class': 'form-control', 'readonly':True}),
            'receive_date' : DateInput(attrs={'class': 'form-control', 'readonly':True}),
            'warranty_startdate' : DateInput(attrs={'class': 'form-control'}),
            'warranty_expiredate' : DateInput(attrs={'class': 'form-control'}),
            'qty' : forms.NumberInput(attrs={'class': 'form-control'}),
            'min_qty' : forms.NumberInput(attrs={'class': 'form-control'}),
            

        }

class AccessorieIssueForm(ModelForm):
    class Meta:
        model = AccessoriesIssue
        fields = ['people_id','accessorie_no','assigned_qty']
        widgets = {           
            'people_id' : forms.Select(attrs={'class': 'form-control'}),
            'accessorie_no' : forms.TextInput(attrs={'class': 'form-control'}),
            'assigned_qty' : forms.TextInput(attrs={'class': 'form-control'}),
               
        }

class ComponentCreateForm(ModelForm):
    def clean_image(self):
        file = self.cleaned_data.get("image", False)
        if file:
            ext = file.name.split('.')[-1].lower()
            if ext not in ["jpg", "jpeg", "png"]:
                raise forms.ValidationError("อนุญาตเฉพาะไฟล์ jpg, jpeg และ png เท่านั้น")

        return file
    class Meta:
        model = Component
        fields = ['component_name', 'manufacturer', 'location', 'brand', 'model' , 'serial', 'suppiler',
                  'purchase_cost', 'invoice_number', 'receive_date', 'warranty_startdate', 'warranty_expiredate',
                  'qty', 'min_qty', 'image']
        widgets = {
            'component_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'manufacturer' : forms.Select(attrs={'class': 'form-control'}),
            'location' : forms.Select(attrs={'class': 'form-control'}),
            'brand' : forms.Select(attrs={'class': 'form-control'}),
            'model' : forms.TextInput(attrs={'class': 'form-control'}),
            'serial' : forms.TextInput(attrs={'class': 'form-control'}),
            'suppiler' : forms.Select(attrs={'class': 'form-control'}),
            'purchase_cost' : forms.NumberInput(attrs={'class': 'form-control'}),
            'invoice_number' : forms.TextInput(attrs={'class': 'form-control'}),
            'receive_date' : DateInput(attrs={'class': 'form-control'}),
            'warranty_startdate' : DateInput(attrs={'class': 'form-control'}),
            'warranty_expiredate' : DateInput(attrs={'class': 'form-control'}),
            'qty' : forms.NumberInput(attrs={'class': 'form-control'}),
            'min_qty' : forms.NumberInput(attrs={'class': 'form-control'}),
            'image' : forms.ClearableFileInput(attrs={'class': 'form-control'}),

        }

class ComponentIssueForm(ModelForm):
    class Meta:
        model = ComponentsIssue
        fields = ['people_id','assigned_qty']
        widgets = {           
            'people_id' : forms.Select(attrs={'class': 'form-control'}),
            'assigned_qty' : forms.TextInput(attrs={'class': 'form-control'}),
        }

class ConsumableViewForm(ModelForm):
    class Meta:
        model = Component
        fields = ['itemcode','component_name', 'manufacturer', 'location', 'brand', 'model' , 'serial', 'suppiler',
                  'purchase_cost', 'invoice_number', 'receive_date', 'warranty_startdate', 'warranty_expiredate',
                  'qty', 'min_qty', 'image']
        widgets = {
            'itemcode' : forms.Select(attrs={'class': 'form-control'}),
            'component_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'manufacturer' : forms.Select(attrs={'class': 'form-control'}),
            'location' : forms.Select(attrs={'class': 'form-control'}),
            'brand' : forms.Select(attrs={'class': 'form-control'}),
            'model' : forms.TextInput(attrs={'class': 'form-control'}),
            'serial' : forms.TextInput(attrs={'class': 'form-control'}),
            'suppiler' : forms.Select(attrs={'class': 'form-control'}),
            'purchase_cost' : forms.NumberInput(attrs={'class': 'form-control'}),
            'invoice_number' : forms.TextInput(attrs={'class': 'form-control'}),
            'receive_date' : DateInput(attrs={'class': 'form-control'}),
            'warranty_startdate' : DateInput(attrs={'class': 'form-control'}),
            'warranty_expiredate' : DateInput(attrs={'class': 'form-control'}),
            'qty' : forms.NumberInput(attrs={'class': 'form-control'}),
            'min_qty' : forms.NumberInput(attrs={'class': 'form-control'}),
            'image' : forms.ClearableFileInput(attrs={'class': 'form-control'}),

        }

class AssetCreateForm(ModelForm):
    def clean_image(self):
        file = self.cleaned_data.get("image", False)
        if file:
            ext = file.name.split('.')[-1].lower()
            if ext not in ["jpg", "jpeg", "png"]:
                raise forms.ValidationError("อนุญาตเฉพาะไฟล์ jpg, jpeg และ png เท่านั้น")

        return file
    class Meta:
        model = Asset
        fields = ['item_code','asset_type','asset_no','model','serial','brand','asset_name',
                  'location','ip_address_1','ip_address_2','mac_address_lan_1','mac_address_lan_2','mac_address_wifi','suppiler','purchase_date','purchase_cost',
                  'invoice_number','receive_date','warranty_startdate','warranty_expiredate','status','image','fix_asset_no',
                  'type_location','frequency_pm']
        widgets = {
            'item_code' : forms.Select(attrs={'class': 'form-control'}),
            'asset_type' : forms.Select(attrs={'class': 'form-control'}),
            'asset_no' : forms.TextInput(attrs={'class': 'form-control'}),
            'asset_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'model' : forms.TextInput(attrs={'class': 'form-control'}),
            'serial' : forms.TextInput(attrs={'class': 'form-control'}),
            'brand' : forms.Select(attrs={'class': 'form-control'}),
            'location' : forms.Select(attrs={'class': 'form-control'}),
            'ip_address_1' : forms.TextInput(attrs={'class': 'form-control'}),
            'ip_address_2' : forms.TextInput(attrs={'class': 'form-control'}),
            'mac_address_lan_1' : forms.TextInput(attrs={'class': 'form-control'}),
            'mac_address_lan_2' : forms.TextInput(attrs={'class': 'form-control'}),
            'mac_address_wifi' : forms.TextInput(attrs={'class': 'form-control'}),
            'suppiler' : forms.Select(attrs={'class': 'form-control'}),
            'purchase_date' : DateInput(attrs={'class': 'form-control'}),
            'purchase_cost' : forms.NumberInput(attrs={'class': 'form-control'}),
            'invoice_number' : forms.TextInput(attrs={'class': 'form-control'}),
            'receive_date' : DateInput(attrs={'class': 'form-control'}),
            'warranty_startdate' : DateInput(attrs={'class': 'form-control'}),
            'warranty_expiredate' : DateInput(attrs={'class': 'form-control'}),
            'status' : forms.Select(attrs={'class': 'form-control'}),
            'image' : forms.ClearableFileInput(attrs={'class': 'form-control'}),
            'fix_asset_no' : forms.TextInput(attrs={'class': 'form-control'}),
            'type_location' : forms.Select(attrs={'class': 'form-control'}),
            'frequency_pm' : forms.Select(attrs={'class': 'form-control'}),

        }

class AssetViewForm(ModelForm):
    class Meta:
        model = Asset
        fields = ['item_code','asset_type','asset_no','model','serial','brand','asset_name',
                  'location','ip_address_1','ip_address_2','mac_address_lan_1','mac_address_lan_2','mac_address_wifi','suppiler','purchase_date','purchase_cost',
                  'invoice_number','receive_date','warranty_startdate','warranty_expiredate','status','image','fix_asset_no',
                  'type_location','pm_date_next','importance_score', 'servicelife_score', 'environment_score']

                      

        widgets = {
            'item_code' : forms.Select(attrs={'class': 'form-control', 'readonly':True}),
            'asset_type' : forms.Select(attrs={'class': 'form-control', 'readonly':True}),
            'asset_no' : forms.TextInput(attrs={'class': 'form-control'}),
            'asset_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'model' : forms.TextInput(attrs={'class': 'form-control'}),
            'serial' : forms.TextInput(attrs={'class': 'form-control'}),
            'brand' : forms.Select(attrs={'class': 'form-control'}),          
            'location' : forms.Select(attrs={'class': 'form-control'}),
            'ip_address_1' : forms.TextInput(attrs={'class': 'form-control'}),
            'ip_address_2' : forms.TextInput(attrs={'class': 'form-control'}),
            'mac_address_lan_1' : forms.TextInput(attrs={'class': 'form-control'}),
            'mac_address_lan_2' : forms.TextInput(attrs={'class': 'form-control'}),
            'mac_address_wifi' : forms.TextInput(attrs={'class': 'form-control'}),
            'suppiler' : forms.Select(attrs={'class': 'form-control'}),
            'purchase_date' : DateInput(attrs={'class': 'form-control'}),
            'purchase_cost' : forms.NumberInput(attrs={'class': 'form-control'}),
            'invoice_number' : forms.TextInput(attrs={'class': 'form-control'}),
            'receive_date' : DateInput(attrs={'class': 'form-control'}),
            'warranty_startdate' : DateInput(attrs={'class': 'form-control'}),
            'warranty_expiredate' : DateInput(attrs={'class': 'form-control'}),
            'status' : forms.Select(attrs={'class': 'form-control'}),
            'image' : forms.ClearableFileInput(attrs={'class': 'form-control'}),
            'fix_asset_no' : forms.TextInput(attrs={'class': 'form-control'}),
            'type_location' : forms.Select(attrs={'class': 'form-control'}),
            'pm_date_next' : DateInput(attrs={'class': 'form-control'}),

            'importance_score' : forms.NumberInput(attrs={'class': 'form-control'}),
            'servicelife_score' : forms.NumberInput(attrs={'class': 'form-control'}),
            'environment_score' : forms.NumberInput(attrs={'class': 'form-control'}),            
            
        }

class AssetIssueForm(ModelForm):
    class Meta:
        model = AssetIssue
        fields = ['user_id','checked_date']
        widgets = {           
            'user_id' : forms.Select(attrs={'class': 'form-control'}),
            'checked_date' : DateInput(attrs={'class': 'form-control'}),
        }

class SoftwareAssetForm(ModelForm):
    class Meta:
        model = SoftwareForAsset
        fields = ['os_name','os_std','office','office_std','browser','browser_std','email','email_std','helpdesk',
                  'helpdesk_std','lineapp','line_std','antivirus','antivirus_std','mta','ax','software_Special1',
                  'special_check1','program_name1','software_Special2','special_check2','program_name2',
                  'software_Special3','special_check3','program_name3','software_Special4','special_check4',
                  'program_name4','software_Special5','special_check5','program_name5','mouse','keyboard',
                  'adaptor','gigbag']
        widgets = {
            'os_name' : forms.CheckboxInput(attrs={'class': ''}),
            'os_std' : forms.Select(attrs={'class': 'form-control'}),
            'office' : forms.CheckboxInput(attrs={'class': ''}),
            'office_std' : forms.Select(attrs={'class': 'form-control'}),
            'browser' : forms.CheckboxInput(attrs={'class': ''}),
            'browser_std' : forms.TextInput(attrs={'class': 'form-control', 'value': "Google chrome"}),
            'email' : forms.CheckboxInput(attrs={'class': ''}),
            'email_std' : forms.TextInput(attrs={'class': 'form-control', 'value': "Zimbra Mail"}),
            'helpdesk' : forms.CheckboxInput(attrs={'class': ''}),
            'helpdesk_std' : forms.TextInput(attrs={'class': 'form-control', 'value': "HRC/IT Helpdesk/PMII"}),
            'lineapp' : forms.CheckboxInput(attrs={'class': ''}),
            'line_std' : forms.TextInput(attrs={'class': 'form-control', 'value': "LinePC"}),
            'antivirus' : forms.CheckboxInput(attrs={'class': ''}),
            'antivirus_std' : forms.Select(attrs={'class': 'form-control'}),
            'mta' : forms.CheckboxInput(attrs={'class': ''}),
            'ax' : forms.CheckboxInput(attrs={'class': ''}),

            'software_Special1' : forms.Select(attrs={'class': 'form-control'}),
            'special_check1' : forms.CheckboxInput(attrs={'class': ''}),
            'program_name1' : forms.TextInput(attrs={'class': 'form-control'}),
            'software_Special2' : forms.Select(attrs={'class': 'form-control'}),
            'special_check2' : forms.CheckboxInput(attrs={'class': ''}),
            'program_name2' : forms.TextInput(attrs={'class': 'form-control'}),
            'software_Special3' : forms.Select(attrs={'class': 'form-control'}),
            'special_check3' : forms.CheckboxInput(attrs={'class': ''}),
            'program_name3' : forms.TextInput(attrs={'class': 'form-control'}),
            'software_Special4' : forms.Select(attrs={'class': 'form-control'}),
            'special_check4' : forms.CheckboxInput(attrs={'class': ''}),
            'program_name4' : forms.TextInput(attrs={'class': 'form-control'}),
            'software_Special5' : forms.Select(attrs={'class': 'form-control'}),
            'special_check5' : forms.CheckboxInput(attrs={'class': ''}),
            'program_name5' : forms.TextInput(attrs={'class': 'form-control'}),

            'mouse' : forms.CheckboxInput(attrs={'class': ''}),
            'keyboard' : forms.CheckboxInput(attrs={'class': ''}),
            'adaptor' : forms.CheckboxInput(attrs={'class': ''}),
            'gigbag' : forms.CheckboxInput(attrs={'class': ''}),


        }

class MaintenanceCreateForm(ModelForm):
    class Meta:
        model = AssetMaintenance
        fields = ['asset_no','location','maintenance_type','title','start_date','compietion_date','notes',
                  'waranty','cost']
        widgets = {
            'asset_no' : forms.Select(attrs={'class': 'form-control'}),
            'location' : forms.Select(attrs={'class': 'form-control'}),
            'maintenance_type' : forms.Select(attrs={'class': 'form-control'}),
            'title' : forms.TextInput(attrs={'class': 'form-control'}),
            'start_date' : DateInput(attrs={'class': 'form-control'}),
            'compietion_date' : DateInput(attrs={'class': 'form-control'}),
            'notes' : forms.Textarea(attrs={'class': 'form-control'}),
            'waranty' : forms.TextInput(attrs={'class': 'form-control'}),
            'cost' : forms.NumberInput(attrs={'class': 'form-control'}),

        }

class MaintenanceViewForm(ModelForm):
    class Meta:
        model = AssetMaintenance
        fields = ['asset_no','location', 'maintenance_type', 'title', 'start_date', 'compietion_date', 
                  'notes', 'waranty','cost']
        widgets = {
            'asset_no' : forms.TextInput(attrs={'class': 'form-control', 'readonly':True}),
            'location' : forms.Select(attrs={'class': 'form-control'}),
            'maintenance_type' : forms.Select(attrs={'class': 'form-control'}),
            'title' : forms.TextInput(attrs={'class': 'form-control'}),
            'start_date' : DateInput(attrs={'class': 'form-control'}),
            'compietion_date' : DateInput(attrs={'class': 'form-control'}),
            'notes' : forms.TextInput(attrs={'class': 'form-control'}),
            'waranty' : forms.TextInput(attrs={'class': 'form-control'}),
            'cost' : forms.NumberInput(attrs={'class': 'form-control'}),
        }

class PMComputerForm(ModelForm):
    class Meta:
        model = PreventiveComputerSuccess
        fields = ['pm_date', 'pm_by', 'comment_pm', 'os', 'health_status','remark']
        widgets = {
            # 'asset_id' : forms.Textarea(attrs={'class': 'form-control'}),
            'pm_date' : DateInput(attrs={'class': 'form-control'}),
            'pm_by' : forms.Select(attrs={'class': 'form-control'}),
            'comment_pm' : forms.Select(attrs={'class': 'form-control'}),
            'os' : forms.Textarea(attrs={'class': 'form-control'}),
            'health_status' : forms.Select(attrs={'class': 'form-control'}),
            'remark' : forms.Textarea(attrs={'class': 'form-control'}),
        }

class PMComputerViewForm(ModelForm):
    class Meta:
        model = PreventiveComputerSuccess
        fields = ['pm_date', 'pm_by', 'comment_pm', 'os', 'health_status','remark']
        widgets = {
            # 'asset_id' : forms.TextInput(attrs={'class': 'form-control'}),
            'pm_date' : DateInput(attrs={'class': 'form-control', 'readonly':True}),
            'pm_by' : forms.Select(attrs={'class': 'form-control'}),
            'comment_pm' : forms.Select(attrs={'class': 'form-control', 'readonly':True}),
            'os' : forms.Textarea(attrs={'class': 'form-control'}),
            'health_status' : forms.Select(attrs={'class': 'form-control', 'readonly':True}),
            'remark' : forms.Textarea(attrs={'class': 'form-control'}),
        }

class LeasesCreateForm(ModelForm):
    class Meta:
        model = Leases
        fields = ['leases_type', 'leases_name', 'description', 'document_no', 'seriel_number','qty',
                  'owner_email', 'contract_period', 'manufacturer', 'suppiler', 'purchase_cost',
                  'po_no', 'receive_date', 'warranty_startdate', 'warranty_expiredate']
        widgets = {
            'leases_type' : forms.Select(attrs={'class': 'form-control'}),
            'leases_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'description' : forms.Textarea(attrs={'class': 'form-control'}),
            'document_no' : forms.TextInput(attrs={'class': 'form-control'}),
            'seriel_number' : forms.TextInput(attrs={'class': 'form-control'}),
            'qty' : forms.NumberInput(attrs={'class': 'form-control'}),
            'owner_email' : forms.TextInput(attrs={'class': 'form-control'}),
            'contract_period' : forms.NumberInput(attrs={'class': 'form-control'}),
            'manufacturer' : forms.Select(attrs={'class': 'form-control'}),
            'suppiler' : forms.Select(attrs={'class': 'form-control'}),
            'purchase_cost' : forms.NumberInput(attrs={'class': 'form-control'}),
            'po_no' : forms.TextInput(attrs={'class': 'form-control'}),
            'receive_date' : DateInput(attrs={'class': 'form-control'}),
            'warranty_startdate' : DateInput(attrs={'class': 'form-control'}),
            'warranty_expiredate' : DateInput(attrs={'class': 'form-control'}),
        }

class LeasesViewForm(ModelForm):
    class Meta:
        model = Leases
        fields = ['leases_type', 'leases_name', 'description', 'document_no', 'seriel_number','qty',
                  'owner_email', 'contract_period', 'manufacturer', 'suppiler', 'purchase_cost',
                  'po_no', 'receive_date', 'warranty_startdate', 'warranty_expiredate']
        widgets = {
            'leases_type' : forms.Select(attrs={'class': 'form-control'}),
            'leases_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'description' : forms.Textarea(attrs={'class': 'form-control'}),
            'document_no' : forms.TextInput(attrs={'class': 'form-control'}),
            'seriel_number' : forms.TextInput(attrs={'class': 'form-control'}),
            'qty' : forms.NumberInput(attrs={'class': 'form-control'}),
            'owner_email' : forms.TextInput(attrs={'class': 'form-control'}),
            'contract_period' : forms.NumberInput(attrs={'class': 'form-control'}),
            'manufacturer' : forms.Select(attrs={'class': 'form-control'}),
            'suppiler' : forms.Select(attrs={'class': 'form-control'}),
            'purchase_cost' : forms.NumberInput(attrs={'class': 'form-control'}),
            'po_no' : forms.TextInput(attrs={'class': 'form-control'}),
            'receive_date' : DateInput(attrs={'class': 'form-control'}),
            'warranty_startdate' : DateInput(attrs={'class': 'form-control'}),
            'warranty_expiredate' : DateInput(attrs={'class': 'form-control'}),
        }