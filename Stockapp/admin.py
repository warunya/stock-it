from django.contrib import admin
from import_export.admin import ImportExportModelAdmin    
from import_export import resources
from .models import Department,People,Location,Asset,Manufacturer,Depreciation,ItemcodeMaster,AssetMaintenance,Supplier,List_Of_Software,SoftwareForAsset


# Register your models here.

admin.site.register(Department)
admin.site.register(Location)
admin.site.register(AssetMaintenance)
admin.site.register(SoftwareForAsset)

class ListSoftwareAdmin(admin.ModelAdmin):
    list_display = ('name')
admin.site.register(List_Of_Software)

class ManufacturerAdmin(admin.ModelAdmin):
    list_display = ('name', 'url')
admin.site.register(Manufacturer)


class PeopleAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'position', 'department')
admin.site.register(People, PeopleAdmin)

class AssetModel(admin.ModelAdmin):
    list_display = ('item_code', 'asset_name')
admin.site.register(Asset, AssetModel)

class DepreciationModel(admin.ModelAdmin):
    list_display = ('depreciation_name','number_of_month')
admin.site.register(Depreciation,DepreciationModel)

class ItemcodeMasterAdmin(admin.ModelAdmin):
    list_display = ('item_code','item_name','category')
admin.site.register(ItemcodeMaster,ItemcodeMasterAdmin)

class SupplierAdmin(admin.ModelAdmin):
    list_display = ('supplier_name','address','mobile_phone','office_phone','fax','email')
admin.site.register(Supplier,SupplierAdmin)






