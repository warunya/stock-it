from django.shortcuts import render, redirect , get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from datetime import date, timedelta, datetime
from django.db.models import Avg, Max, Min, Sum

from .models import Leases,Manufacturer,Supplier
from .forms import (LeasesCreateForm,LeasesViewForm)

def createleases(request):
    if request.method == 'GET':
        return render(request, 'stockapp/create_leases.html', 
        {
            'form': LeasesCreateForm(),
            'pageTitle': 'Create new Leases (สัญญาเช่าใช้)',
        })
    else:
        try:
            form = LeasesCreateForm(request.POST, request.FILES)
            if form.is_valid():
                newdata = form.save(commit=False)
                newdata.request_user = request.user
                newdata.save()
                return redirect('viewallleases')                
            else:
                return render(request, 'stockapp/create_leases.html', {'form': LeasesCreateForm(), 'error': form.errors})

        except ValueError as error:
            return render(request, 'stockapp/create_leases.html', {'form': LeasesCreateForm(), 'error': error}) 

def viewallleases(request):
    leases = Leases.objects.all()
    current_day = date.today()
    detailleases = []

    for item in leases:   
        diff = item.warranty_expiredate - current_day
        status = diff.days 
        # combine 
        detailleases.append({
            'id': item.id,
            'leasestype_choices': item.leasestype_choices,
            'leases_name': item.leases_name,
            'description': item.description,
            'qty': item.qty,
            'contract_period' : item.contract_period,
            'manufacturer': item.manufacturer,
            'suppiler': item.suppiler,
            'warranty_startdate': item.warranty_startdate,
            'warranty_expiredate': item.warranty_expiredate,
            'status': status,
          
        })
    return render(request, 'stockapp/allleases.html', {'pageTitle': 'All Leases (สัญญาเช่าใช้)','leases' : detailleases}) 

def viewallleasesprinter(request):
    leases = Leases.objects.filter(leases_type=1)
    current_day = date.today()
    detailleases = []

    for item in leases:  
        diff = item.warranty_expiredate - current_day
        status = diff.days   
        
        detailleases.append({
            'id': item.id,
            'leasestype_choices': item.leasestype_choices,
            'leases_name': item.leases_name,
            'description': item.description,
            'qty': item.qty,
            'contract_period' : item.contract_period,
            'suppiler': item.suppiler,
            'warranty_startdate': item.warranty_startdate,
            'warranty_expiredate': item.warranty_expiredate,
            'status': status,
        })
    return render(request, 'stockapp/allleases.html', {'pageTitle': 'All Leases (สัญญาเช่าใช้)','leases' : detailleases}) 

def viewallleaseslicenses(request):
    leases = Leases.objects.filter(leases_type=2)
    current_day = date.today()
    detailleases = []

    for item in leases:   
        diff = item.warranty_expiredate - current_day
        status = diff.days  
        # combine 
        detailleases.append({
            'id': item.id,
            'leasestype_choices': item.leasestype_choices,
            'leases_name': item.leases_name,
            'description': item.description,
            'qty': item.qty,
            'contract_period' : item.contract_period,
            'manufacturer': item.manufacturer,
            'suppiler': item.suppiler,
            'warranty_startdate': item.warranty_startdate,
            'warranty_expiredate': item.warranty_expiredate,
            'status': status,
          
        })
    return render(request, 'stockapp/allleases.html', {'pageTitle': 'All Leases (สัญญาเช่าใช้)','leases' : detailleases}) 

def viewallleasesline(request):
    leases = Leases.objects.filter(leases_type=3)
    current_day = date.today()
    detailleases = []

    for item in leases:  
        diff = item.warranty_expiredate - current_day
        status = diff.days   
        # combine 
        detailleases.append({
            'id': item.id,
            'leasestype_choices': item.leasestype_choices,
            'leases_name': item.leases_name,
            'description': item.description,
            'qty': item.qty,
            'contract_period' : item.contract_period,
            'manufacturer': item.manufacturer,
            'suppiler': item.suppiler,
            'warranty_startdate': item.warranty_startdate,
            'warranty_expiredate': item.warranty_expiredate,
            'status': status,
          
        })
    return render(request, 'stockapp/allleases.html', {'pageTitle': 'All Leases (สัญญาเช่าใช้)','leases' : detailleases}) 

def viewallleasesdomain(request):
    leases = Leases.objects.filter(leases_type=4)
    current_day = date.today()
    detailleases = []

    for item in leases:   
        diff = item.warranty_expiredate - current_day
        status = diff.days  
        # combine 
        detailleases.append({
            'id': item.id,
            'leasestype_choices': item.leasestype_choices,
            'leases_name': item.leases_name,
            'description': item.description,
            'qty': item.qty,
            'contract_period' : item.contract_period,
            'manufacturer': item.manufacturer,
            'suppiler': item.suppiler,
            'warranty_startdate': item.warranty_startdate,
            'warranty_expiredate': item.warranty_expiredate,
            'status': status,
          
        })
    return render(request, 'stockapp/allleases.html', {'pageTitle': 'All Leases (สัญญาเช่าใช้)','leases' : detailleases}) 

def viewleasesdetail(request, leases_pk):    

    item = get_object_or_404(Leases, pk=leases_pk)

    if request.method == 'GET':
        form = LeasesViewForm(instance=item)
        return render(request, 'stockapp/viewleases.html', {'pageTitle': 'View Leases Detail', 'item': item, 'form': form})
    else:
        try:
            form = LeasesViewForm(request.POST, instance=item)
            form.save()
            return redirect('viewallleases')
        except ValueError as error:
            return render(request, 'stockapp/viewleases.html', {'pageTitle': 'View Leases Detail', 'item': item, 'form': form, 'error': error})