from django.shortcuts import render, redirect , get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.db import IntegrityError,transaction
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from datetime import date, timedelta  
from dateutil.relativedelta import *
from django.db.models import Avg, Max, Min, Sum
from .models import ItemcodeMaster,ConsumablesIssue,AccessoriesIssue,ComponentsIssue,Consumable,Accessorie,Component,BFstock

from django.db.models import Q

def allstocksummary(request):
    itemcodes = ItemcodeMaster.objects.all()

    if request.method == 'GET':

        return render(request, 'stockapp/summary_stock.html')

    elif request.method == 'POST':
        start_date = request.POST.get("startdate")
        end_date = request.POST.get("enddate")

        ########################## Receive ########################
        receive_consumable = Consumable.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=0)

        receive_accessorie = Accessorie.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=0)

        receive_component = Component.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=0)

        ######################### Issue ###########################
        issue_consumable = ConsumablesIssue.objects.values('consumable_id__itemcode').annotate(sum=Sum('assigned_qty')).filter(created_at__range=(start_date,end_date))

        issue_accessorie = AccessoriesIssue.objects.values('accessorie_id__itemcode').annotate(sum=Sum('assigned_qty')).filter(created_at__range=(start_date,end_date))

        issue_component = ComponentsIssue.objects.values('component_id__itemcode').annotate(sum=Sum('assigned_qty')).filter(created_at__range=(start_date,end_date))
        
        ######################### BF ###########################
        bf_consumable = Consumable.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=1)
        bf_accessorie = Accessorie.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=1)
        bf_component = Component.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=1)
        
        detailsummarystock = []

        for item in itemcodes :

            receive_qty = 0
            for i in receive_consumable:
                if (item.id == i['itemcode_id']):
                    receive_qty = i['sum']
                    break;
            
            detailsummarystock.append({
                'id': item.id,
                'itemnumber': item.item_code,
                'item_name': item.item_name,
                'category': item.category_choices_value,
                'bf': 0,
                'receive_qty': receive_qty,
                'issued' : 0,
                'balance': 0,

            })

        
####################################################### receive ###############################################################
# Loop for receive_accessorie ------------------------------
        # for item in detailsummarystock: 
        #     for i in receive_accessorie:
        #         if (item['id'] == i['itemcode_id']):
        #             item['receive_qty'] = i['sum']
        #             break        
# End Loop for receive_accessorie ----------------------------

# Loop for receive_component ----------------------------------
        # for item in detailsummarystock:
        #     for i in receive_component:
        #         print ( i['itemcode_id'] )
        #         if (item['id'] == i['itemcode_id']):   
        #             item['receive_qty'] = i['sum']
        #             break
#End Loop for receive_component ------------------------------

# ####################################################### Issue ###############################################################

# #Loop for issue_consumable
#         for item in detailsummarystock:
#             for i in issue_consumable:
#                 if (item['id'] == i['consumable_id__itemcode']):   
#                     item['issued'] = i['sum']
#                     item['balance'] = (item['bf'] + item['receive_qty']) - item['issued']
#                     break
# #End for issue_consumable--------------------------------------

# #Loop for issue_accessorie
#         for item in detailsummarystock:
#             for i in issue_accessorie:
#                 if (item['id'] == i['accessorie_id__itemcode']):   
#                     item['issued'] = i['sum']
#                     item['balance'] = (item['bf'] + item['receive_qty']) - item['issued']
#                     break
# #End for issue_accessorie--------------------------------------

# #Loop for issue_component
#         for item in detailsummarystock:
#             for i in issue_component:
#                 if (item['id'] == i['component_id__itemcode']):   
#                     item['issued'] = i['sum']
#                     item['balance'] = (item['bf'] + item['receive_qty']) - item['issued']
#                     break
# #End for issue_component--------------------------------------

####################################################### BF ###############################################################
#         for item in detailsummarystock:
#             for i in bf_consumable:
#                 if (item['id'] == i['itemcode_id']):   
#                     item['bf'] = i['sum']
#                     item['balance'] = (item['bf'] + item['receive_qty']) - item['issued']
#                     break
# #--------------------------------------------------
#         for item in detailsummarystock:
#             for i in bf_accessorie:
#                 if (item['id'] == i['itemcode_id']):   
#                     item['bf'] = i['sum']
#                     item['balance'] = (item['bf'] + item['receive_qty']) - item['issued']
#                     break
# #--------------------------------------------------

#         for item in detailsummarystock:
#             for i in bf_component:
#                 if (item['id'] == i['itemcode_id']):   
#                     item['bf'] = i['sum']
#                     item['balance'] = (item['bf'] + item['receive_qty']) - item['issued']
#                     break
#--------------------------------------------------

        return render(request, 'stockapp/summary_stock.html',{
            'pageTitle': 'Stock Balance',
            'itemcodes': detailsummarystock,
        })

def summarizmonth(request):
    itemcodes = ItemcodeMaster.objects.all()
    if request.method == 'GET':
        return render(request, 'stockapp/summarize_month.html')
    elif request.method == 'POST':
        start_date = request.POST.get("startdate")
        end_date = request.POST.get("enddate")

        ########################## Receive ########################
        receive_consumable = Consumable.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=0)

        receive_accessorie = Accessorie.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=0)

        receive_component = Component.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=0)

        ######################### Issue ###########################
        issue_consumable = ConsumablesIssue.objects.values('consumable_id__itemcode').annotate(sum=Sum('assigned_qty')).filter(created_at__range=(start_date,end_date))

        issue_accessorie = AccessoriesIssue.objects.values('accessorie_id__itemcode').annotate(sum=Sum('assigned_qty')).filter(created_at__range=(start_date,end_date))

        issue_component = ComponentsIssue.objects.values('component_id__itemcode').annotate(sum=Sum('assigned_qty')).filter(created_at__range=(start_date,end_date))
        
        ######################### BF ###########################
        # bf_consumable = Consumable.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=1)
        # bf_accessorie = Accessorie.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=1)
        # bf_component = Component.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=1)
        
        detailsummarystock = []

        for item in itemcodes :

            receive_comsumable_qty = 0
            for i in receive_consumable:
                if (item.id == i['itemcode_id']):
                    receive_comsumable_qty = i['sum']
                    print(item.id,receive_comsumable_qty)
                    break;
            
            detailsummarystock.append({
                'id': item.id,
                'itemnumber': item.item_code,
                'item_name': item.item_name,
                'category': item.category_choices_value,
                'bf': 0,
                'receive_comsumable_qty': receive_comsumable_qty,
                'issued' : 0,
                'balance': 0,

            })
####################################################### receive ###############################################################
# Loop for receive_accessorie ------------------------------
        for item in detailsummarystock:
                
            for i in receive_accessorie:
                if (item['id'] == i['itemcode_id']):
                    item['receive_comsumable_qty'] = i['sum']
                    break        
# End Loop for receive_accessorie ----------------------------

# Loop for receive_component ----------------------------------
        for item in detailsummarystock:
            for i in receive_component:
                if (item['id'] == i['itemcode_id']):   
                    item['receive_comsumable_qty'] = i['sum']
                    break
#End Loop for receive_component ------------------------------

####################################################### Issue ###############################################################

#Loop for issue_consumable
        for item in detailsummarystock:
            for i in issue_consumable:
                if (item['id'] == i['consumable_id__itemcode']):   
                    item['issued'] = i['sum']
                    item['balance'] = (item['bf'] + item['receive_comsumable_qty']) - item['issued']
                    break
#End for issue_consumable--------------------------------------

#Loop for issue_accessorie
        for item in detailsummarystock:
            for i in issue_accessorie:
                if (item['id'] == i['accessorie_id__itemcode']):   
                    item['issued'] = i['sum']
                    item['balance'] = (item['bf'] + item['receive_comsumable_qty']) - item['issued']
                    break
#End for issue_accessorie--------------------------------------

#Loop for issue_component
        for item in detailsummarystock:
            for i in issue_component:
                if (item['id'] == i['component_id__itemcode']):   
                    item['issued'] = i['sum']
                    item['balance'] = (item['bf'] + item['receive_comsumable_qty']) - item['issued']
                    break
#End for issue_component--------------------------------------

####################################################### BF ###############################################################
#         for item in detailsummarystock:
#             for i in bf_consumable:
#                 if (item['id'] == i['itemcode_id']):   
#                     item['bf'] = i['sum']
#                     item['balance'] = (item['bf'] + item['receive_comsumable_qty']) - item['issued']
#                     break
# #--------------------------------------------------
#         for item in detailsummarystock:
#             for i in bf_accessorie:
#                 if (item['id'] == i['itemcode_id']):   
#                     item['bf'] = i['sum']
#                     item['balance'] = (item['bf'] + item['receive_comsumable_qty']) - item['issued']
#                     break
# #--------------------------------------------------

#         for item in detailsummarystock:
#             for i in bf_component:
#                 if (item['id'] == i['itemcode_id']):   
#                     item['bf'] = i['sum']
#                     item['balance'] = (item['bf'] + item['receive_comsumable_qty']) - item['issued']
#                     break
#--------------------------------------------------

        return render(request, 'stockapp/summarize_month.html',{
            'pageTitle': 'Stock Balance',
            'itemcodes': detailsummarystock,
        })

def insertbalance(request):
    itemcodes = ItemcodeMaster.objects.all()
    if request.method == 'GET':
        return render(request, 'stockapp/summarize_month.html')
    elif request.method == 'POST':
        start_date = request.POST.get("startdate")
        end_date = request.POST.get("enddate")

        ########################## Receive ########################
        receive_consumable = Consumable.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=0)

        receive_accessorie = Accessorie.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=0)

        receive_component = Component.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=0)

        ######################### Issue ###########################
        issue_consumable = ConsumablesIssue.objects.values('consumable_id__itemcode').annotate(sum=Sum('assigned_qty')).filter(created_at__range=(start_date,end_date))

        issue_accessorie = AccessoriesIssue.objects.values('accessorie_id__itemcode').annotate(sum=Sum('assigned_qty')).filter(created_at__range=(start_date,end_date))

        issue_component = ComponentsIssue.objects.values('component_id__itemcode').annotate(sum=Sum('assigned_qty')).filter(created_at__range=(start_date,end_date))
        
        ######################### BF ###########################
        # bf_consumable = Consumable.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=1)
        # bf_accessorie = Accessorie.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=1)
        # bf_component = Component.objects.values('itemcode_id').annotate(sum=Sum('qty')).filter(receive_date__range=(start_date,end_date),status=1)
        
        detailsummarystock = []

        for item in itemcodes :

            receive_comsumable_qty = 0
            for i in receive_consumable:
                if (item.id == i['itemcode_id']):
                    receive_comsumable_qty = i['sum']
                    print(item.id,receive_comsumable_qty)
                    break;
            
            detailsummarystock.append({
                'id': item.id,
                'itemnumber': item.item_code,
                'item_name': item.item_name,
                'category': item.category_choices_value,
                'bf': 0,
                'receive_comsumable_qty': receive_comsumable_qty,
                'issued' : 0,
                'balance': 0,

            })
####################################################### receive ###############################################################
# Loop for receive_accessorie ------------------------------
        for item in detailsummarystock:
                
            for i in receive_accessorie:
                if (item['id'] == i['itemcode_id']):
                    item['receive_comsumable_qty'] = i['sum']
                    break        
# End Loop for receive_accessorie ----------------------------

# Loop for receive_component ----------------------------------
        for item in detailsummarystock:
            for i in receive_component:
                if (item['id'] == i['itemcode_id']):   
                    item['receive_comsumable_qty'] = i['sum']
                    break
#End Loop for receive_component ------------------------------

####################################################### Issue ###############################################################

#Loop for issue_consumable
        for item in detailsummarystock:
            for i in issue_consumable:
                if (item['id'] == i['consumable_id__itemcode']):   
                    item['issued'] = i['sum']
                    item['balance'] = (item['bf'] + item['receive_comsumable_qty']) - item['issued']
                    break
#End for issue_consumable--------------------------------------

#Loop for issue_accessorie
        for item in detailsummarystock:
            for i in issue_accessorie:
                if (item['id'] == i['accessorie_id__itemcode']):   
                    item['issued'] = i['sum']
                    item['balance'] = (item['bf'] + item['receive_comsumable_qty']) - item['issued']
                    break
#End for issue_accessorie--------------------------------------

#Loop for issue_component
        for item in detailsummarystock:
            for i in issue_component:
                if (item['id'] == i['component_id__itemcode']):   
                    item['issued'] = i['sum']
                    item['balance'] = (item['bf'] + item['receive_comsumable_qty']) - item['issued']
                    break
#End for issue_component--------------------------------------

####################################################### BF ###############################################################
#         for item in detailsummarystock:
#             for i in bf_consumable:
#                 if (item['id'] == i['itemcode_id']):   
#                     item['bf'] = i['sum']
#                     item['balance'] = (item['bf'] + item['receive_comsumable_qty']) - item['issued']
#                     break
# #--------------------------------------------------
#         for item in detailsummarystock:
#             for i in bf_accessorie:
#                 if (item['id'] == i['itemcode_id']):   
#                     item['bf'] = i['sum']
#                     item['balance'] = (item['bf'] + item['receive_comsumable_qty']) - item['issued']
#                     break
# #--------------------------------------------------

#         for item in detailsummarystock:
#             for i in bf_component:
#                 if (item['id'] == i['itemcode_id']):   
#                     item['bf'] = i['sum']
#                     item['balance'] = (item['bf'] + item['receive_comsumable_qty']) - item['issued']
#                     break
#--------------------------------------------------
        return render(request, 'stockapp/summarize_month.html',{
            'pageTitle': 'Stock Balance',
            'itemcodes': detailsummarystock,
        })
        # for data in detailsummarystock:
        #     balance = BFstock.objects.create(
        #         itemcode=item.item_code, 
        #         qty=item.balance, 
        #         bf_date=end_date + timedelta(days=1),
        #         create_at=timezone.now()
        #     )
        #     balance.save()
        # redirect('summarizmonth')
        

    