# Generated by Django 3.0.6 on 2020-09-14 09:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('Stockapp', '0038_auto_20200910_1322'),
    ]

    operations = [
        migrations.AddField(
            model_name='asset',
            name='frequency_pm',
            field=models.IntegerField(blank=True, choices=[(1, '1 Month'), (2, '2 Month'), (3, '3 Month'), (6, '6 Month'), (12, '1 Year')], null=True),
        ),
        migrations.AddField(
            model_name='asset',
            name='pm_date_next',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='asset',
            name='type_location',
            field=models.IntegerField(choices=[(1, 'Production'), (2, 'Operation'), (3, 'Office')], default=1),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='accessorie',
            name='manufacturer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='Stockapp.Manufacturer'),
        ),
        migrations.AlterField(
            model_name='assetmaintenance',
            name='asset_no',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='Stockapp.Asset', verbose_name='Asset No'),
        ),
        migrations.AlterField(
            model_name='assetmaintenance',
            name='maintenance_type',
            field=models.IntegerField(choices=[(1, 'Upgrade'), (2, 'Maintenance'), (3, 'Repair'), (4, 'Software Support'), (5, 'Calibration')], verbose_name='Maintenance Type'),
        ),
        migrations.AlterField(
            model_name='assetmaintenance',
            name='title',
            field=models.CharField(blank=True, max_length=200, null=True, verbose_name='Title'),
        ),
        migrations.AlterField(
            model_name='component',
            name='manufacturer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='Stockapp.Manufacturer'),
        ),
        migrations.AlterField(
            model_name='consumable',
            name='manufacturer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='Stockapp.Manufacturer'),
        ),
        migrations.AlterField(
            model_name='licenses',
            name='manufacturer',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='Stockapp.Manufacturer'),
        ),
    ]
