# Generated by Django 3.0.6 on 2020-08-25 06:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('Stockapp', '0009_auto_20200825_1157'),
    ]

    operations = [
        migrations.AddField(
            model_name='asset',
            name='status',
            field=models.IntegerField(choices=[(0, 'Damage'), (1, 'Ready')], default=1),
        ),
        migrations.AlterField(
            model_name='asset',
            name='asset_category',
            field=models.IntegerField(choices=[(1, 'Asset'), (2, 'License'), (3, 'Accessie'), (4, 'Consumable'), (5, 'Component')]),
        ),
        migrations.AlterField(
            model_name='asset',
            name='asset_type',
            field=models.IntegerField(blank=True, choices=[(1, 'Computer'), (2, 'Laptop'), (3, 'Printer')], null=True),
        ),
        migrations.AlterField(
            model_name='asset',
            name='brand',
            field=models.CharField(blank=True, choices=[('HP', 'HP'), ('Lenovo', 'Lenovo'), ('Dell', 'Dell'), ('Acer', 'Acer')], max_length=100, null=True),
        ),
    ]
