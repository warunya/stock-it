from django.shortcuts import render, redirect , get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from datetime import date
from dateutil.relativedelta import *
from django.db.models import Avg, Max, Min, Sum

from django.db.models import Q # Query OR

from .models import People,Department,ItemcodeMaster,List_Of_Software,Manufacturer,Supplier,Location,AssetMaintenance, Asset, AssetIssue, People, Licenses

######################### User ####################################

def alluserbydepartment(request):
    departments = Department.objects.all()

    if request.method == 'GET':
        data = People.objects.all()
        departments = Department.objects.all()
        return render(request, 'stockapp/master_all_userbydep.html', 
        {
            'pageTitle': 'All User in domain By Department',
            'departments': departments,
            'users': data,
        })            
    elif request.method == "POST":
            id_department = request.POST.get("id_department")
            data = People.objects.filter(department=id_department)
            # start_date = request.POST.get("startdate")
            # end_date = request.POST.get("enddate")

            # code = request.POST.get('code') # OR Searching

            # data = Person.objects.filter(Q(assstno=code) | Q(serialno=code) | Q(first_name=code)) # OR            

            # date = xxx.objects.filter(bd__range=(start_date, end_date), name=x)  Between 
            return render(request, 'stockapp/master_all_userbydep.html', 
            {
                'pageTitle': 'All User in domain By Department',
                'departments': departments,
                'users': data,
                'current_dept_id' : int(id_department)
            })
        #     else:
        #         return (request, 'stockapp/master_all_userbydep.html',{ 'pageTitle': 'Error'})
        # except ValueError as error:
        #      return render(request, 'stockapp/master_all_userbydep.html', {'error': error}) 

def alluserbydate(request):
    if request.method == 'GET':
        users = People.objects.all()

        return render(request, 'stockapp/report_all_createuser.html', 
        {
            'pageTitle': 'All User in domain By CreateDate user',
            'users': users
        })  
    elif request.method == "POST":
        start_date = request.POST.get("startdate")
        end_date = request.POST.get("enddate")
        users = People.objects.filter(created_at__range=(start_date, end_date))
        return render(request, 'stockapp/report_all_createuser.html',
        {
            'pageTitle': 'All User in domain By CreateDate user',
            'users': users,
            'current_startdate': start_date,
            'current_enddate': end_date,
        })
       
################################## Asset ############################################
def allrepairasset(request):
    maintenances = AssetMaintenance.objects.all()
    if request.method == 'GET':
        return render(request, 'stockapp/report_all_repairasset.html',
        {
            'pageTitle': 'All Maintenance Asset',
            'maintenances': maintenances,
        })
    elif request.method == 'POST':
        start_date = request.POST.get("startdate")
        end_date = request.POST.get("enddate")
        maintenances = AssetMaintenance.objects.filter(start_date__range=(start_date, end_date))
        return render(request, 'stockapp/report_all_repairasset.html',
        {
            'pageTitle': 'All Maintenance Asset By Start Date',
            'maintenances': maintenances,

        })

###################################### Hardward Inventory #######################################
def hardwardinventory(request):
    allasset = Asset.objects.values('id','check_in_out').filter(status=1).distinct()
    today = date.today()

    allusers = []
    for item in allasset:
        issue = AssetIssue.objects.filter(asset_id=item['id']).aggregate(Max('id'))

        try:
            last_user  = AssetIssue.objects.get(id=issue['id__max'])
            age = relativedelta(today, last_user.asset_id.warranty_startdate)
            year = age.years
            month = age.months
            day = age.days
            diff = last_user.asset_id.warranty_expiredate - today
            diff_date = diff.days
            
            if item['check_in_out'] == 0:
                username  = '%s %s' %(last_user.user_id.first_name, last_user.user_id.last_name)
            else:
                username = '-'
            # firstname  = last_user.user_id.first_name
            # lastname   = last_user.user_id.last_name
            department = last_user.user_id.department
            asset_id   = last_user.asset_id.id            
            asset_type = last_user.asset_id.get_asset_type_display()
            model      = last_user.asset_id.model  
            brand      = last_user.asset_id.brand
            serial     = last_user.asset_id.serial
            asset_name = last_user.asset_id.asset_name
            ip_address_1 = last_user.asset_id.ip_address_1
            mac_address_lan_1 = last_user.asset_id.mac_address_lan_1
            mac_address_wifi  = last_user.asset_id.mac_address_wifi
            fix_asset  = last_user.asset_id.fix_asset_no
            status     = last_user.asset_id.assetstatus_type
            warranty_startdate  = last_user.asset_id.warranty_startdate
            warranty_expiredate = last_user.asset_id.warranty_expiredate

            #....other info.      
        except AssetIssue.DoesNotExist:
            try:
                find_asset_item = Asset.objects.get(id=int(item['id']))

                asset_id   = find_asset_item.id
                asset_type = find_asset_item.get_asset_type_display()
                model      = find_asset_item.model
                brand      = find_asset_item.brand
                serial     = find_asset_item.serial
                asset_name = find_asset_item.asset_name
                ip_address_1 = find_asset_item.ip_address_1
                mac_address_lan_1 = find_asset_item.mac_address_lan_1
                mac_address_wifi  = find_asset_item.mac_address_wifi
                fix_asset  = find_asset_item.fix_asset_no
                status     = find_asset_item.assetstatus_type
                warranty_startdate = find_asset_item.warranty_startdate
                warranty_expiredate = find_asset_item.warranty_expiredate
                # firstname  = '-'
                # lastname   = '-'
                username   = '-'
                department = 'MIS'
                age = relativedelta(today, find_asset_item.warranty_startdate)
                year = age.years
                month = age.months
                day = age.days
                diff = find_asset_item.warranty_expiredate - today
                diff_date = diff.days

            except Asset.DoesNotExist:
                asset_id   = 0
                asset_type = '-'
                model      = '-'
                asset_name = '-'
                # firstname  = '-'
                # lastname   = '-'
                username   = '-'
                department = 'MIS'     
                year = '-'
                month = '-'
                day = '-'
                diff_date = '-'  
                         
        if diff_date < 0 :
            warranty_status = 'NO'
        else:
            warranty_status = 'Yes'
       
        allusers.append({           
            'asset_id'  : asset_id,
            'asset_type': asset_type,
            'model'     : model,
            'department': department,
            'brand'     : brand,
            'serial'    : serial,
            'asset_name': asset_name,
            'ip_address_1': ip_address_1,
            'mac_address_wifi': mac_address_wifi,
            'mac_address_lan_1': mac_address_lan_1,
            'fix_asset' : fix_asset,
            'status': status,
            'warranty_startdate': warranty_startdate,
            'warranty_expiredate': warranty_expiredate,
            'username'  : username,
            'year': year,
            'month': month,
            'day': day,
            'diff_date': diff_date,
            'warranty_status': warranty_status,

            #...other info           
        })  

    return render(request, 'stockapp/report_hardware_inventory.html',
    {
        'pageTitle': 'All Hardware Inventory',
        'allusers': allusers,
    })

def allnormalasset(request):
    assets = Asset.objects.filter(status=1)
    return render(request, 'stockapp/report_all_normalasset.html',
    {
        'pageTitle': 'All Normal Asset (สถานะ : ใช้งานได้ปกติ)' ,
        'assets': assets,
    })

def alldamageasset(request):
    assets = Asset.objects.filter(status=2)
    return render(request, 'stockapp/report_all_normalasset.html',
    {
        'pageTitle': 'All Normal Asset (สถานะ : ใช้งานไม่ได้)' ,
        'assets': assets,
    })

def allmaintenanceasset(request):
    if request.method == 'GET':
        assets = AssetMaintenance.objects.all()

        return render(request, 'stockapp/report_all_maintenance.html', 
        {
            'pageTitle': 'All Maintenances',
            'assets': assets
        })  
    elif request.method == "POST":
        start_date = request.POST.get("startdate")
        end_date = request.POST.get("enddate")
        assets = AssetMaintenance.objects.filter(compietion_date__range=(start_date, end_date))
        return render(request, 'stockapp/report_all_maintenance.html',
        {
            'pageTitle': 'All Maintenances By SearchDate',
            'assets': assets,
            'current_startdate': start_date,
            'current_enddate': end_date,
        })

def alllicenses(request):
    licenses = Licenses.objects.all()
    # expire_object = Licenses.objects.values('warranty_expiredate')
    return render(request, 'stockapp/report_all_licenses.html',
    {
        'pageTitle': 'All Licenses' ,
        'licenses': licenses,
    })
    