from django.urls import path
from Stockapp import (views, views_accessorie, views_asset, views_component, views_consumable, views_license,
views_maintenance,views_preventive,views_leases, views_masterdata, views_pdf, views_reportdata, views_stock)
from django.contrib import admin

urlpatterns = [
    path('', views.index, name='index'),

    #Auth
    #path('signup/', views.signupuser , name='signupuser'),
    path('login/', views.loginuser, name='loginuser'),
    path('logout/', views.logoutuser, name='logoutuser'),

    path('licenses/create', views_license.createlicenses, name='createlicenses'),
    path('licenses/all', views_license.viewalllicenses, name='viewalllicenses'),
    path('viewlicenses/<int:licenses_pk>', views_license.viewlicenses, name='viewlicenses'),

    path('consumable/create', views_consumable.createconsumable, name='createconsumable'),
    path('consumable/all', views_consumable.viewallconsumable, name='viewallconsumable'),
    path('viewcomsumable/<int:consumable_pk>', views_consumable.viewconsumable, name='viewconsumable'),

    path('accessorie/create', views_accessorie.createaccessorie, name='createaccessorie'),
    path('accessorie/all', views_accessorie.viewallaccessorie, name='viewallaccessorie'),
    path('accessorie/<int:accessorie_pk>', views_accessorie.viewaccessorie, name='viewaccessorie'),

    path('component/create', views_component.createcomponent, name='createcomponent'),
    path('component/all', views_component.viewallcomponent, name='viewallcomponent'),
    path('component/<int:component_pk>', views_component.viewcomponent, name='viewcomponent'),

    path('asset/create', views_asset.createasset, name='createasset'),
    path('asset/all', views_asset.viewallasset, name='viewallasset'),
    path('asset/<int:asset_pk>', views_asset.viewasset, name='viewasset'),

    path('issueasset/all', views_asset.viewallissueasset, name='viewallissueasset'),

    path('checkout/<int:asset_pk>', views_asset.checkout_asset, name='checkout_asset'),

    # Issue
    path('issueaccessorie/<int:accessorie_pk>', views_accessorie.IssueAccessorieUser, name='IssueAccessorieUser'),
    path('issuecomsumable/<int:consumable_pk>', views_consumable.IssueConsumableUser, name='IssueConsumableUser'),
    path('issuecomponent/<int:component_pk>', views_component.IssueComponentUser, name='IssueComponentUser'),
    path('issueasset/<int:asset_pk>', views_asset.IssueAssetUser, name='IssueAssetUser'),
    path('issueassetsoftware/<int:asset_pk>', views_asset.AssetForSoftware, name='AssetForSoftware'),

    # Asset Type
    path('printer/all', views_asset.viewallprinter, name='viewallprinter'),
    path('computer/all', views_asset.viewallcomputer, name='viewallcomputer'),
    path('laptop/all', views_asset.viewalllaptop, name='viewalllaptop'),
    path('switch/all', views_asset.viewallswitch, name='viewallswitch'),
    path('monitor/all', views_asset.viewallmonitor, name='viewallmonitor'),
    path('accesspoint/all', views_asset.viewallaccesspoint, name='viewallaccesspoint'),

    #Maintenance
    path('maintenance/all', views_maintenance.viewallmaintenance, name='viewallmaintenance'),
    path('maintenance/create', views_maintenance.createmaintenance, name='createmaintenance'),
    path('maintenance/<int:maintenance_pk>', views_maintenance.viewmaintenancedetail, name='viewmaintenancedetail'),

    #Preventive
    path('preventive/allasset', views_preventive.allassetpm, name='allassetpm'),
    path('preventive/<int:asset_pk>', views_preventive.viewassetdetail, name='viewassetdetail'),
    path('preventive/allassetpm', views_preventive.allassetpmatdate, name='allassetpmatdate'),
    path('preventive/successpm', views_preventive.pmsuccess, name='pmsuccess'),
    path('preventive/allplanbymonth', views_preventive.allplanpmbymonth, name='allplanpmbymonth'),

    path('preventive/allpmcomputer', views_preventive.allpmcomputer, name='allpmcomputer'),
    path('preventivecomputer/<int:asset_pk>', views_preventive.pmcomputersuccess, name='pmcomputersuccess'),
    path('preventivecomputerfinish', views_preventive.allpmcomputersuccess, name='allpmcomputersuccess'),
    path('preventivecomputerfinish/<int:pm_pk>', views_preventive.viewpmcomputersuccessdetail, name='viewpmcomputersuccessdetail'),

    path('preventlaptop/allpmlaptop', views_preventive.allpmlaptop, name='allpmlaptop'),

    path('preventiveprinter/allpmprinter', views_preventive.allpmprinter, name='allpmprinter'),

    #Leases
    path('leases/alllease', views_leases.viewallleases, name='viewallleases'),
    path('leases/allleaseprinter', views_leases.viewallleasesprinter, name='viewallleasesprinter'),
    path('leases/allleaselicenses', views_leases.viewallleaseslicenses, name='viewallleaseslicenses'),
    path('leases/allleaseline', views_leases.viewallleasesline, name='viewallleasesline'),
    path('leases/allleasedomain', views_leases.viewallleasesdomain, name='viewallleasesdomain'),
    path('leases/create', views_leases.createleases, name='createleases'),
    path('leases/<int:leases_pk>', views_leases.viewleasesdetail, name='viewleasesdetail'),

    #Master Data
    path('masteruser/alluser', views_masterdata.viewalluser, name='viewalluser'),
    path('masteruser/createuser', views_masterdata.createuser, name='createuser'),
    path('masteruser/<int:user_pk>', views_masterdata.viewuserdetail, name='viewuserdetail'),

    path('masterdepartment/alldepartment', views_masterdata.viewalldepartment, name='viewalldepartment'),
    path('masterdepartment/createdepartment', views_masterdata.createdepartment, name='createdepartment'),
    path('masterdepartment/<int:department_pk>', views_masterdata.viewdepartmentdetail, name='viewdepartmentdetail'),

    path('masteritemcode/allitemcode', views_masterdata.viewallitemcode, name='viewallitemcode'),
    path('masteritemcode/createitemcode', views_masterdata.createitemcode, name='createitemcode'),
    path('masteritemcode/<int:itemcode_pk>', views_masterdata.viewitemcodedetail, name='viewitemcodedetail'),

    path('mastersoftware/allsoftware', views_masterdata.viewallsoftware, name='viewallsoftware'),
    path('mastersoftware/createsoftware', views_masterdata.createsoftware, name='createsoftware'),
    path('mastersoftware/<int:software_pk>', views_masterdata.viewsoftwaredetail, name='viewsoftwaredetail'),

    path('mastermanufacturer/allmanufacturer', views_masterdata.viewallmanufacturer, name='viewallmanufacturer'),
    path('mastermanufacturer/createmanufacturer', views_masterdata.createmanufacturer, name='createmanufacturer'),
    path('mastermanufacturer/<int:manufacturer_pk>', views_masterdata.viewmanufacturerdetail, name='viewmanufacturerdetail'),

    path('mastersupplier/allsupplier', views_masterdata.viewallsupplier, name='viewallsupplier'),
    path('mastersupplier/createsupplier', views_masterdata.createsupplier, name='createsupplier'),
    path('mastersupplier/<int:supplier_pk>', views_masterdata.viewsupplierdetail, name='viewsupplierdetail'),

    path('masterlocation/alllocation', views_masterdata.viewalllocation, name='viewalllocation'),
    path('masterlocation/createlocation', views_masterdata.createlocation, name='createlocation'),
    path('masterlocation/<int:location_pk>', views_masterdata.viewlocationdetail, name='viewlocationdetail'),

    # Create PDF
    path('PDFuserdomain/<str:username>', views_pdf.GeneratePDFUserDomain, name='GeneratePDFUserDomain'),
    path('PDFPMComputer/<int:id>/<int:asset_id>', views_pdf.GeneratePDFPMComputer, name='GeneratePDFPMComputer'),
    path('PDFComputerIssue/<int:asset_id>/<int:issued_id>', views_pdf.GeneratePDFComputerReceiveReturn, name='GeneratePDFComputerReceiveReturn'),

    # Report 
    path('reportuser/alluserbydep', views_reportdata.alluserbydepartment, name='alluserbydepartment'),
    path('reportuser/createuser', views_reportdata.alluserbydate, name='alluserbydate'),
    path('reportasset/allrepair', views_reportdata.allrepairasset, name='allrepairasset'),
    path('reportasset/hardwardinventory', views_reportdata.hardwardinventory, name='hardwardinventory'),
    path('reportasset/normalasset', views_reportdata.allnormalasset, name='allnormalasset'),
    path('reportasset/damageasset', views_reportdata.alldamageasset, name='alldamageasset'),
    path('reportasset/maintenances', views_reportdata.allmaintenanceasset, name='allmaintenanceasset'),
    path('reportasset/licenses', views_reportdata.alllicenses, name='alllicenses'),

    # Stock
    path('stock/balance', views_stock.allstocksummary, name='allstocksummary'),
    path('stock/summarizemonth', views_stock.summarizmonth, name='summarizmonth'),
    path('stock/insertbalance', views_stock.insertbalance, name='insertbalance'),

]