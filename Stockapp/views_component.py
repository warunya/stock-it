from django.shortcuts import render, redirect , get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.db.models import Avg, Max, Min, Sum

from .models import Component,ItemcodeMaster,ComponentsIssue
from .forms import (ConsumableIssueForm,ComponentIssueForm,ComponentCreateForm,ConsumableViewForm)

def createcomponent(request):
    if request.method == 'GET':
        componentitems = ItemcodeMaster.objects.filter(category=5)

        return render(request, 'stockapp/createcomponent.html',
        {            
            'form': ComponentCreateForm(),
            'pageTitle': 'Create new Component',                
            'componentitems': componentitems
        })
    else:
        try:
            form = ComponentCreateForm(request.POST, request.FILES)
            if form.is_valid():
                newdata = form.save(commit=False)
                iteminstant = ItemcodeMaster.objects.get(id=request.POST['itemcode'])
                newdata.itemcode = iteminstant
                
                newdata.request_user = request.user
                newdata.save()
                return redirect('viewallcomponent')                
            else:
                return render(request, 'stockapp/createcomponent.html',
                {
                    'form': ComponentCreateForm(),
                     'error': form.errors
                })

        except ValueError as error:
            return render(request, 'stockapp/createcomponent.html', {'form': ComponentCreateForm(), 'error': error}) 

def viewallcomponent(request):
    components = Component.objects.all()

    sub_component_obj = ComponentsIssue.objects.values('component_id').annotate(sum=Sum('assigned_qty')) # Query หนเดียวแล้ว*
    detailcomponent = []

    for item in components:    
        # combine
        
        component_qty = 0 #* ใช้ Loop เพื่อหา Sum ด้วย id
        for i in sub_component_obj: # find sum in QuerySet
            if (item.id == i['component_id']):
                component_qty = i['sum']
                print(component_qty)
                break
        # combine
        detailcomponent.append({
            'id': item.id,
            'itemcode': item.itemcode,
            'consumable_name': item.component_name,
            'consumable_catagory': item.location,
            'brand': item.brand,
            'model': item.model,
            'qty': item.qty,
            'consumable_qty' : component_qty,
            'balance' : item.qty - component_qty,
            
        })

    return render(request, 'stockapp/allcomponent.html', {'pageTitle': 'All Component', 'components': detailcomponent}) 
    
def viewcomponent(request, component_pk):    

    item = get_object_or_404(Component, pk=component_pk)

    if request.method == 'GET':
        form = ConsumableViewForm(instance=item)
        return render(request, 'stockapp/viewcomponent.html', {'pageTitle': 'View Component Detail', 'item': item, 'form': form})
    else:
        try:
            form = ConsumableViewForm(request.POST, instance=item)
            form.save()
            return redirect('viewallcomponent')
        except ValueError as error:
            return render(request, 'stockapp/viewcomponent.html', {'pageTitle': 'View Component Detail', 'item': item, 'form': form, 'error': error})  

def IssueComponentUser(request, component_pk):
    item = get_object_or_404(Component, pk=component_pk)

    if request.method == 'GET':
        item_name_obj = Component.objects.get(id = component_pk)
        return render(request, 'stockapp/issue_component.html', 
        {
            'form': ComponentIssueForm(), 
            'pageTitle': 'Issue Component Form',
            'item_name': item_name_obj.itemcode,
           
        })
    else:
        try:
            form = ComponentIssueForm(request.POST)
            if form.is_valid():
                newdata = form.save(commit=False)

                instance_Component = Component.objects.get(id = component_pk)
                newdata.component_id = instance_Component      #ตัวแปร.ฟิลใน model ComponentIssue = inatance

                newdata.created_at = timezone.now()
                newdata.save()
                return redirect('viewallcomponent')                
            else:
                return render(request, 'stockapp/issue_component.html', {'form': ComponentIssueForm(), 'error': form.errors})

        except ValueError as error:
            return render(request, 'stockapp/issue_component.html', {'form': ComponentIssueForm(), 'error': error})
