#import...

def calcuate_frequency_pm(importance_score, servicelife_score, environment_score):
    x = importance_score * servicelife_score * environment_score
    if x >=19:
        result = 1 # Grade A, 1 คือ 1 เดือน
    elif (x >= 13 and x <= 18):
        result = 3 # Grade B, 3 เดือน
    elif (x >= 7 and x <= 12):
        result = 6 # Grade C, 6 เดือน
    else:
        result = 12 # Grade D, 12 เดือน
    return result
