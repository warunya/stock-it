from django.shortcuts import render, redirect , get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.utils import timezone
from django.db.models import Avg, Max, Min, Sum

from .models import Accessorie, AccessoriesIssue, ItemcodeMaster
from .forms import (AccessorieCreateForm,AccessorieViewForm,AccessorieIssueForm)

def createaccessorie(request):
    if request.method == 'GET':

        accessorieitems = ItemcodeMaster.objects.filter(category=3)

        return render(request, 'stockapp/createaccessorie.html', {
            'form': AccessorieCreateForm(),
            'pageTitle': 'Create new Accessorie',
            'accessorieitems': accessorieitems
            })
    else:
        try:
            form = AccessorieCreateForm(request.POST, request.FILES)
            if form.is_valid():               
                newdata = form.save(commit=False)

                iteminstant = ItemcodeMaster.objects.get(id=request.POST['itemcode'])
                newdata.itemcode = iteminstant
                
                newdata.request_user = request.user
                newdata.save()
                return redirect('viewallaccessorie')                
            else:
                return render(request, 'stockapp/createaccessorie.html', {'form': AccessorieCreateForm(), 'error': form.errors})

        except ValueError as error:
            return render(request, 'stockapp/createaccessorie.html', {'form': AccessorieCreateForm(), 'error': error}) 

def viewaccessorie(request, accessorie_pk):    

    item = get_object_or_404(Accessorie, pk=accessorie_pk)

    if request.method == 'GET':
        form = AccessorieViewForm(instance=item)
        return render(request, 'stockapp/viewaccessorie.html', {'pageTitle': 'View Accessorie Detail', 'item': item, 'form': form})
    else:
        try:
            form = AccessorieViewForm(request.POST, instance=item)
            form.save()
            return redirect('viewallaccessorie')
        except ValueError as error:
            return render(request, 'stockapp/viewaccessorie.html', {'pageTitle': 'View Accessorie Detail', 'item': item, 'form': form, 'error': error})

def Issueaccessorie(request, accessorie_pk):    

    item = get_object_or_404(Accessorie, pk=accessorie_pk)

    if request.method == 'GET':
        form = AccessorieIssueForm(instance=item)
        return render(request, 'stockapp/issueaccessorie.html', 
        {
            'pageTitle': 'Issue Accesssorie Form',
            'item': item,
            'form': form
        })
    else:
        try:
            form = AccessorieIssueForm(request.POST, instance=item)
            form.save()
            return redirect('viewallaccessorie')
        except ValueError as error:
            return render(request, 'stockapp/viewaccessorie.html', {'pageTitle': 'Issue Accesssorie Form', 'item': item, 'form': form, 'error': error})

def IssueAccessorieUser(request, accessorie_pk):
    item = get_object_or_404(Accessorie, pk=accessorie_pk)

    if request.method == 'GET':
        item_name_obj = Accessorie.objects.get(id = accessorie_pk)
        return render(request, 'stockapp/issue_accessorie_insert.html', 
        {
            'form': AccessorieIssueForm(),
            'pageTitle': 'Issue Accesssorie Form',
            'item_name': item_name_obj.accessorie_name

        })
    else:
        try:
            form = AccessorieIssueForm(request.POST)
            if form.is_valid():
                newdata = form.save(commit=False)

                instance_Accessorie = Accessorie.objects.get(id = accessorie_pk)
                newdata.accessorie_id = instance_Accessorie

                newdata.created_at = timezone.now()
                newdata.save()
                return redirect('viewallaccessorie')                
            else:
                return render(request, 'stockapp/issue_accessorie_insert.html', {'form': AccessorieIssueForm(), 'error': form.errors})

        except ValueError as error:
            return render(request, 'stockapp/issue_accessorie_insert.html', {'form': AccessorieIssueForm(), 'error': error})    

def viewallaccessorie(request):
    accessories = Accessorie.objects.all()

    sub_iss_obj = AccessoriesIssue.objects.values('accessorie_id').annotate(sum=Sum('assigned_qty')) # Query หนเดียวแล้ว*
    detailaccessorie = []

    for item in accessories:    
        # combine
        
        iss_qty = 0 #* ใช้ Loop เพื่อหา Sum ด้วย id
        for i in sub_iss_obj: # find sum in QuerySet
            if (item.id == i['accessorie_id']):
                iss_qty = i['sum']
                print(iss_qty)
                break

        # try: แบบหา Sum โดยทำการ Query ทีละรายการ
        #     iss_qty = AccessoriesIssue.objects.values('accessorie_id').annotate(sum=Sum('assigned_qty')).get(accessorie_id=item.id)["sum"]
        # except AccessoriesIssue.DoesNotExist:
        #     iss_qty = 0

        detailaccessorie.append({
            'id': item.id,
            'itemcode': item.itemcode,
            'accessorie_name': item.accessorie_name,
            'brand': item.brand,
            'model': item.model,
            'qty': item.qty,
            'purchase_cost' : item.purchase_cost,
            'location' : item.location, 
            
            'iss_qty' : iss_qty,
            'balance' : item.qty - iss_qty,
            
        })

    return render(request, 'stockapp/allaccessorie.html', {'pageTitle': 'All Accessorie', 'accessories': detailaccessorie}) 