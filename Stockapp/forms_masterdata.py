from django.forms import ModelForm
from .models import (People,Department,ItemcodeMaster,List_Of_Software,Manufacturer,Supplier,Location)
from django import forms

# User
class MasterUserCreateForm(ModelForm):
    class Meta:
        model = People
        fields = ['username', 'first_name', 'last_name', 'position', 'department', 'division', 'email', 'status']
        widgets = {
            'username' : forms.TextInput(attrs={'class': 'form-control'}),
            'first_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'last_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'position' : forms.TextInput(attrs={'class': 'form-control'}),
            'department' : forms.Select(attrs={'class': 'form-control'}),
            'division' : forms.Select(attrs={'class': 'form-control'}),
            'email' : forms.TextInput(attrs={'class': 'form-control'}),
            'status' : forms.Select(attrs={'class': 'form-control'}),
        }

class MasterUserViewForm(ModelForm):
    class Meta:
        model = People
        fields = ['username', 'first_name', 'last_name', 'position', 'department', 'division', 'email', 'status']
        widgets = {
            'username' : forms.TextInput(attrs={'class': 'form-control'}),
            'first_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'last_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'position' : forms.TextInput(attrs={'class': 'form-control'}),
            'department' : forms.Select(attrs={'class': 'form-control'}),
            'division' : forms.Select(attrs={'class': 'form-control'}),
            'email' : forms.Textarea(attrs={'class': 'form-control'}),
            'status' : forms.Select(attrs={'class': 'form-control'}),
        }

# Department
class MasterDepartmentCreateForm(ModelForm):
    class Meta:
        model = Department
        fields = ['department_name']
        widgets = {
            'department_name' : forms.TextInput(attrs={'class': 'form-control'}),
        }


class MasterDepartmentViewForm(ModelForm):
    class Meta:
        model = Department
        fields = ['department_name']
        widgets = {
            'department_name' : forms.TextInput(attrs={'class': 'form-control'}),
        }

# ItemCode #
class MasterItemCodeCreateForm(ModelForm):
    class Meta:
        model = ItemcodeMaster
        fields = ['item_code', 'item_name', 'category']
        widgets = {
            'item_code' : forms.TextInput(attrs={'class': 'form-control'}),
            'item_name' : forms.Textarea(attrs={'class': 'form-control'}),
            'category' : forms.Select(attrs={'class': 'form-control'}),
        }

class MasterItemCodeViewForm(ModelForm):
    class Meta:
        model = ItemcodeMaster
        fields = ['item_code', 'item_name', 'category']
        widgets = {
            'item_code' : forms.TextInput(attrs={'class': 'form-control'}),
            'item_name' : forms.Textarea(attrs={'class': 'form-control'}),
            'category' : forms.Select(attrs={'class': 'form-control'}),
        }

# SoftwareList
class MasterSoftwareCreateForm(ModelForm):
    class Meta:
        model = List_Of_Software
        fields = ['name']
        widgets = {
            'name' : forms.Textarea(attrs={'class': 'form-control'}),
        }

class MasterSoftwareViewForm(ModelForm):
    class Meta:
        model = List_Of_Software
        fields = ['name']
        widgets = {
            'name' : forms.Textarea(attrs={'class': 'form-control'}),
        }

class MasterManufacturerCreateForm(ModelForm):
    def clean_image(self):
        file = self.cleaned_data.get("image", False)
        if file:
            ext = file.name.split('.')[-1].lower()
            if ext not in ["jpg", "jpeg", "png"]:
                raise forms.ValidationError("อนุญาตเฉพาะไฟล์ jpg, jpeg และ png เท่านั้น")

        return file

    class Meta:
        model = Manufacturer
        fields = ['name', 'support_url', 'support_phone', 'support_email', 'image']
        widgets = {
            'name' : forms.Textarea(attrs={'class': 'form-control'}),
            'support_url' : forms.TextInput(attrs={'class': 'form-control'}),
            'support_phone' : forms.TextInput(attrs={'class': 'form-control'}),
            'support_email' : forms.TextInput(attrs={'class': 'form-control'}),
            'image' : forms.ClearableFileInput(attrs={'class': 'form-control'}),
        }

class MasterManufacturerViewForm(ModelForm):
    def clean_image(self):
        file = self.cleaned_data.get("image", False)
        if file:
            ext = file.name.split('.')[-1].lower()
            if ext not in ["jpg", "jpeg", "png"]:
                raise forms.ValidationError("อนุญาตเฉพาะไฟล์ jpg, jpeg และ png เท่านั้น")

        return file
    class Meta:
        model = Manufacturer
        fields = ['name', 'support_url', 'support_phone', 'support_email', 'image']
        widgets = {
            'name' : forms.Textarea(attrs={'class': 'form-control'}),
            'support_url' : forms.TextInput(attrs={'class': 'form-control'}),
            'support_phone' : forms.TextInput(attrs={'class': 'form-control'}),
            'support_email' : forms.TextInput(attrs={'class': 'form-control'}),
            'image' : forms.ClearableFileInput(attrs={'class': 'form-control'}),
        }

class MasterSupplierCreateForm(ModelForm):
    class Meta:
        model = Supplier
        fields = ['supplier_name', 'address', 'mobile_phone', 'office_phone', 'fax', 'email', 'term_of_payment']
        widgets = {
            'supplier_name' : forms.Textarea(attrs={'class': 'form-control'}),
            'address' : forms.Textarea(attrs={'class': 'form-control'}),
            'mobile_phone' : forms.TextInput(attrs={'class': 'form-control'}),
            'office_phone' : forms.TextInput(attrs={'class': 'form-control'}),
            'fax' : forms.TextInput(attrs={'class': 'form-control'}),
            'email' : forms.TextInput(attrs={'class': 'form-control'}),
            'term_of_payment' : forms.Select(attrs={'class': 'form-control'}),
        }

class MasterSupplierViewForm(ModelForm):
    class Meta:
        model = Supplier
        fields = ['supplier_name', 'address', 'mobile_phone', 'office_phone', 'fax', 'email', 'term_of_payment']
        widgets = {
            'supplier_name' : forms.Textarea(attrs={'class': 'form-control'}),
            'address' : forms.Textarea(attrs={'class': 'form-control'}),
            'mobile_phone' : forms.TextInput(attrs={'class': 'form-control'}),
            'office_phone' : forms.TextInput(attrs={'class': 'form-control'}),
            'fax' : forms.TextInput(attrs={'class': 'form-control'}),
            'email' : forms.TextInput(attrs={'class': 'form-control'}),
            'term_of_payment' : forms.Select(attrs={'class': 'form-control'}),
        }

class MasterLocationCreateForm(ModelForm):
    class Meta:
        model = Location
        fields = ['location_name', 'sub_location_name']
        widgets = {
            'location_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'sub_location_name' : forms.TextInput(attrs={'class': 'form-control'}),
        }

class MasterLocationViewForm(ModelForm):
    class Meta:
        model = Location
        fields = ['location_name', 'sub_location_name']
        widgets = {
            'location_name' : forms.TextInput(attrs={'class': 'form-control'}),
            'sub_location_name' : forms.TextInput(attrs={'class': 'form-control'}),
        }
         
