from django.shortcuts import render, redirect , get_object_or_404
from django.http import HttpResponse
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.decorators import login_required
from django.utils import timezone

# Create your views here.
def index(request):
    return render(request, 'stockapp/dashboard.html')

def loginuser(request):
    if request.method == 'GET':
        return render(request, 'stockapp/login.html', {'form': AuthenticationForm()})
    else:
        # Login User
        user = authenticate(
            request, username=request.POST['username'], password=request.POST['password'])
        if user is None:
            return render(request, 'stockapp/login.html', {'form': AuthenticationForm(), 'error': "Username and password didn't match."})
        else:
            login(request, user)
            return redirect('index')

def logoutuser(request):
    if request.method == 'POST':
        logout(request)
        return redirect('loginuser')

@login_required
def dashboard(request):

    if request.user == True:
        return render(request, 'stockapp/dashboard.html',{'pageTitle':'Dashboard'})
    else:
        return redirect('index')

