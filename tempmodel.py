# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class Accessories(models.Model):
    name = models.CharField(max_length=191, blank=True, null=True)
    category_id = models.IntegerField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    qty = models.IntegerField()
    requestable = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    location_id = models.IntegerField(blank=True, null=True)
    purchase_date = models.DateField(blank=True, null=True)
    purchase_cost = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    order_number = models.CharField(max_length=191, blank=True, null=True)
    company_id = models.PositiveIntegerField(blank=True, null=True)
    min_amt = models.IntegerField(blank=True, null=True)
    manufacturer_id = models.IntegerField(blank=True, null=True)
    model_number = models.CharField(max_length=191, blank=True, null=True)
    image = models.CharField(max_length=191, blank=True, null=True)
    supplier_id = models.IntegerField(blank=True, null=True)
    

    class Meta:
        managed = False
        db_table = 'accessories'


class AccessoriesUsers(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    accessory_id = models.IntegerField(blank=True, null=True)
    assigned_to = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'accessories_users'


class ActionLogs(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    action_type = models.CharField(max_length=191)
    target_id = models.IntegerField(blank=True, null=True)
    target_type = models.CharField(max_length=191, blank=True, null=True)
    location_id = models.IntegerField(blank=True, null=True)
    note = models.TextField(blank=True, null=True)
    filename = models.TextField(blank=True, null=True)
    item_type = models.CharField(max_length=191)
    item_id = models.IntegerField()
    expected_checkin = models.DateField(blank=True, null=True)
    accepted_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    thread_id = models.IntegerField(blank=True, null=True)
    company_id = models.IntegerField(blank=True, null=True)
    accept_signature = models.CharField(max_length=100, blank=True, null=True)
    log_meta = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'action_logs'


class AssetLogs(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    action_type = models.CharField(max_length=191)
    asset_id = models.IntegerField()
    checkedout_to = models.IntegerField(blank=True, null=True)
    location_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    asset_type = models.CharField(max_length=100, blank=True, null=True)
    note = models.TextField(blank=True, null=True)
    filename = models.TextField(blank=True, null=True)
    requested_at = models.DateTimeField(blank=True, null=True)
    accepted_at = models.DateTimeField(blank=True, null=True)
    accessory_id = models.IntegerField(blank=True, null=True)
    accepted_id = models.IntegerField(blank=True, null=True)
    consumable_id = models.IntegerField(blank=True, null=True)
    expected_checkin = models.DateField(blank=True, null=True)
    component_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'asset_logs'


class AssetMaintenances(models.Model):
    asset_id = models.PositiveIntegerField()
    supplier_id = models.PositiveIntegerField()
    asset_maintenance_type = models.CharField(max_length=191)
    title = models.CharField(max_length=100)
    is_warranty = models.IntegerField()
    start_date = models.DateField()
    completion_date = models.DateField(blank=True, null=True)
    asset_maintenance_time = models.IntegerField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    cost = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'asset_maintenances'


class AssetUploads(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    filename = models.CharField(max_length=191)
    asset_id = models.IntegerField()
    filenotes = models.CharField(max_length=191, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'asset_uploads'


class Assets(models.Model):
    name = models.CharField(max_length=191, blank=True, null=True)
    asset_tag = models.CharField(max_length=191, blank=True, null=True)
    model_id = models.IntegerField(blank=True, null=True)
    serial = models.CharField(max_length=191, blank=True, null=True)
    purchase_date = models.DateField(blank=True, null=True)
    purchase_cost = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    order_number = models.CharField(max_length=191, blank=True, null=True)
    assigned_to = models.IntegerField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    image = models.TextField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    physical = models.IntegerField()
    deleted_at = models.DateTimeField(blank=True, null=True)
    status_id = models.IntegerField(blank=True, null=True)
    archived = models.IntegerField(blank=True, null=True)
    warranty_months = models.IntegerField(blank=True, null=True)
    depreciate = models.IntegerField(blank=True, null=True)
    supplier_id = models.IntegerField(blank=True, null=True)
    requestable = models.IntegerField()
    rtd_location_id = models.IntegerField(blank=True, null=True)
    field_snipeit_mac_address_1 = models.CharField(db_column='_snipeit_mac_address_1', max_length=191, blank=True, null=True)  # Field renamed because it started with '_'.
    accepted = models.CharField(max_length=191, blank=True, null=True)
    last_checkout = models.DateTimeField(blank=True, null=True)
    expected_checkin = models.DateField(blank=True, null=True)
    company_id = models.PositiveIntegerField(blank=True, null=True)
    assigned_type = models.CharField(max_length=191, blank=True, null=True)
    last_audit_date = models.DateTimeField(blank=True, null=True)
    next_audit_date = models.DateField(blank=True, null=True)
    location_id = models.IntegerField(blank=True, null=True)
    checkin_counter = models.IntegerField()
    checkout_counter = models.IntegerField()
    requests_counter = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'assets'


class Categories(models.Model):
    name = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    eula_text = models.TextField(blank=True, null=True)
    use_default_eula = models.IntegerField()
    require_acceptance = models.IntegerField()
    category_type = models.CharField(max_length=191, blank=True, null=True)
    checkin_email = models.IntegerField()
    image = models.CharField(max_length=191, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'categories'


class CheckoutRequests(models.Model):
    user_id = models.IntegerField()
    requestable_id = models.IntegerField()
    requestable_type = models.CharField(max_length=191)
    quantity = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    canceled_at = models.DateTimeField(blank=True, null=True)
    fulfilled_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'checkout_requests'


class Companies(models.Model):
    name = models.CharField(unique=True, max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    image = models.CharField(max_length=191, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'companies'


class Components(models.Model):
    name = models.CharField(max_length=191, blank=True, null=True)
    category_id = models.IntegerField(blank=True, null=True)
    location_id = models.IntegerField(blank=True, null=True)
    company_id = models.IntegerField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    qty = models.IntegerField()
    order_number = models.CharField(max_length=191, blank=True, null=True)
    purchase_date = models.DateField(blank=True, null=True)
    purchase_cost = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    min_amt = models.IntegerField(blank=True, null=True)
    serial = models.CharField(max_length=191, blank=True, null=True)
    image = models.CharField(max_length=191, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'components'


class ComponentsAssets(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    assigned_qty = models.IntegerField(blank=True, null=True)
    component_id = models.IntegerField(blank=True, null=True)
    asset_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'components_assets'


class Consumables(models.Model):
    name = models.CharField(max_length=191, blank=True, null=True)
    category_id = models.IntegerField(blank=True, null=True)
    location_id = models.IntegerField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    qty = models.IntegerField()
    requestable = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    purchase_date = models.DateField(blank=True, null=True)
    purchase_cost = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    order_number = models.CharField(max_length=191, blank=True, null=True)
    company_id = models.PositiveIntegerField(blank=True, null=True)
    min_amt = models.IntegerField(blank=True, null=True)
    model_number = models.CharField(max_length=191, blank=True, null=True)
    manufacturer_id = models.IntegerField(blank=True, null=True)
    item_no = models.CharField(max_length=191, blank=True, null=True)
    image = models.CharField(max_length=191, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'consumables'


class ConsumablesUsers(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    consumable_id = models.IntegerField(blank=True, null=True)
    assigned_to = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'consumables_users'


class CustomFieldCustomFieldset(models.Model):
    custom_field_id = models.IntegerField()
    custom_fieldset_id = models.IntegerField()
    order = models.IntegerField()
    required = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'custom_field_custom_fieldset'


class CustomFields(models.Model):
    name = models.CharField(max_length=191)
    format = models.CharField(max_length=191)
    element = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    field_values = models.TextField(blank=True, null=True)
    field_encrypted = models.IntegerField()
    db_column = models.CharField(max_length=191, blank=True, null=True)
    help_text = models.TextField(blank=True, null=True)
    show_in_email = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'custom_fields'


class CustomFieldsets(models.Model):
    name = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'custom_fieldsets'


class Departments(models.Model):
    name = models.CharField(max_length=191)
    user_id = models.IntegerField()
    company_id = models.IntegerField(blank=True, null=True)
    location_id = models.IntegerField(blank=True, null=True)
    manager_id = models.IntegerField(blank=True, null=True)
    notes = models.CharField(max_length=191, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    image = models.CharField(max_length=191, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'departments'


 class Depreciations(models.Model):   #การเลิกใช้งาน
    name = models.CharField(max_length=191)
    months = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'depreciations'


class Imports(models.Model):
    name = models.CharField(max_length=191, blank=True, null=True)
    file_path = models.CharField(max_length=191)
    filesize = models.IntegerField()
    import_type = models.CharField(max_length=191, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    header_row = models.TextField(blank=True, null=True)
    first_row = models.TextField(blank=True, null=True)
    field_map = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'imports'


class LicenseSeats(models.Model):
    license_id = models.IntegerField(blank=True, null=True)
    assigned_to = models.IntegerField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    asset_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'license_seats'


class Licenses(models.Model):
    name = models.CharField(max_length=191, blank=True, null=True)
    serial = models.CharField(max_length=2048, blank=True, null=True)
    purchase_date = models.DateField(blank=True, null=True)
    purchase_cost = models.DecimalField(max_digits=20, decimal_places=2, blank=True, null=True)
    order_number = models.CharField(max_length=50, blank=True, null=True)
    seats = models.IntegerField()
    notes = models.TextField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    depreciation_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    license_name = models.CharField(max_length=120, blank=True, null=True)
    license_email = models.CharField(max_length=191, blank=True, null=True)
    depreciate = models.IntegerField(blank=True, null=True)
    supplier_id = models.IntegerField(blank=True, null=True)
    expiration_date = models.DateField(blank=True, null=True)
    purchase_order = models.CharField(max_length=191, blank=True, null=True)
    termination_date = models.DateField(blank=True, null=True)
    maintained = models.IntegerField(blank=True, null=True)
    reassignable = models.IntegerField()
    company_id = models.PositiveIntegerField(blank=True, null=True)
    manufacturer_id = models.IntegerField(blank=True, null=True)
    category_id = models.IntegerField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'licenses'


class Locations(models.Model):
    name = models.CharField(max_length=191, blank=True, null=True)
    city = models.CharField(max_length=191, blank=True, null=True)
    state = models.CharField(max_length=191, blank=True, null=True)
    country = models.CharField(max_length=191, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    address = models.CharField(max_length=191, blank=True, null=True)
    address2 = models.CharField(max_length=191, blank=True, null=True)
    zip = models.CharField(max_length=10, blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    parent_id = models.IntegerField(blank=True, null=True)
    currency = models.CharField(max_length=10, blank=True, null=True)
    ldap_ou = models.CharField(max_length=191, blank=True, null=True)
    manager_id = models.IntegerField(blank=True, null=True)
    image = models.CharField(max_length=191, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'locations'


class LoginAttempts(models.Model):
    username = models.CharField(max_length=191, blank=True, null=True)
    remote_ip = models.CharField(max_length=45, blank=True, null=True)
    user_agent = models.CharField(max_length=191, blank=True, null=True)
    successful = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'login_attempts'


class Manufacturers(models.Model):
    name = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    url = models.CharField(max_length=191, blank=True, null=True)
    support_url = models.CharField(max_length=191, blank=True, null=True)
    support_phone = models.CharField(max_length=191, blank=True, null=True)
    support_email = models.CharField(max_length=191, blank=True, null=True)
    image = models.CharField(max_length=191, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'manufacturers'


class Migrations(models.Model):
    migration = models.CharField(max_length=191)
    batch = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'migrations'


class Models(models.Model):
    name = models.CharField(max_length=191)
    model_number = models.CharField(max_length=191, blank=True, null=True)
    manufacturer_id = models.IntegerField(blank=True, null=True)
    category_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    depreciation_id = models.IntegerField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    eol = models.IntegerField(blank=True, null=True)
    image = models.CharField(max_length=191, blank=True, null=True)
    deprecated_mac_address = models.IntegerField()
    deleted_at = models.DateTimeField(blank=True, null=True)
    fieldset_id = models.IntegerField(blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    requestable = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'models'


class ModelsCustomFields(models.Model):
    asset_model_id = models.IntegerField()
    custom_field_id = models.IntegerField()
    default_value = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'models_custom_fields'


class OauthAccessTokens(models.Model):
    id = models.CharField(primary_key=True, max_length=100)
    user_id = models.IntegerField(blank=True, null=True)
    client_id = models.IntegerField()
    name = models.CharField(max_length=191, blank=True, null=True)
    scopes = models.TextField(blank=True, null=True)
    revoked = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    expires_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oauth_access_tokens'


class OauthAuthCodes(models.Model):
    id = models.CharField(primary_key=True, max_length=100)
    user_id = models.IntegerField()
    client_id = models.IntegerField()
    scopes = models.TextField(blank=True, null=True)
    revoked = models.IntegerField()
    expires_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oauth_auth_codes'


class OauthClients(models.Model):
    user_id = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=191)
    secret = models.CharField(max_length=100)
    redirect = models.TextField()
    personal_access_client = models.IntegerField()
    password_client = models.IntegerField()
    revoked = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oauth_clients'


class OauthPersonalAccessClients(models.Model):
    client_id = models.IntegerField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oauth_personal_access_clients'


class OauthRefreshTokens(models.Model):
    id = models.CharField(primary_key=True, max_length=100)
    access_token_id = models.CharField(max_length=100)
    revoked = models.IntegerField()
    expires_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'oauth_refresh_tokens'


class PasswordResets(models.Model):
    email = models.CharField(max_length=191)
    token = models.CharField(max_length=191)
    created_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'password_resets'


class PermissionGroups(models.Model):
    name = models.CharField(max_length=191)
    permissions = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'permission_groups'


class RequestedAssets(models.Model):
    asset_id = models.IntegerField()
    user_id = models.IntegerField()
    accepted_at = models.DateTimeField(blank=True, null=True)
    denied_at = models.DateTimeField(blank=True, null=True)
    notes = models.CharField(max_length=191)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'requested_assets'


class Requests(models.Model):
    asset_id = models.IntegerField()
    user_id = models.IntegerField(blank=True, null=True)
    request_code = models.TextField()
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'requests'


class Settings(models.Model):
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    per_page = models.IntegerField()
    site_name = models.CharField(max_length=100)
    qr_code = models.IntegerField(blank=True, null=True)
    qr_text = models.CharField(max_length=32, blank=True, null=True)
    display_asset_name = models.IntegerField(blank=True, null=True)
    display_checkout_date = models.IntegerField(blank=True, null=True)
    display_eol = models.IntegerField(blank=True, null=True)
    auto_increment_assets = models.IntegerField()
    auto_increment_prefix = models.CharField(max_length=191, blank=True, null=True)
    load_remote = models.IntegerField()
    logo = models.CharField(max_length=191, blank=True, null=True)
    header_color = models.CharField(max_length=191, blank=True, null=True)
    alert_email = models.CharField(max_length=191, blank=True, null=True)
    alerts_enabled = models.IntegerField()
    default_eula_text = models.TextField(blank=True, null=True)
    barcode_type = models.CharField(max_length=191, blank=True, null=True)
    slack_endpoint = models.CharField(max_length=191, blank=True, null=True)
    slack_channel = models.CharField(max_length=191, blank=True, null=True)
    slack_botname = models.CharField(max_length=191, blank=True, null=True)
    default_currency = models.CharField(max_length=10, blank=True, null=True)
    custom_css = models.TextField(blank=True, null=True)
    brand = models.IntegerField()
    ldap_enabled = models.CharField(max_length=191, blank=True, null=True)
    ldap_server = models.CharField(max_length=191, blank=True, null=True)
    ldap_uname = models.CharField(max_length=191, blank=True, null=True)
    ldap_pword = models.TextField(blank=True, null=True)
    ldap_basedn = models.CharField(max_length=191, blank=True, null=True)
    ldap_filter = models.TextField(blank=True, null=True)
    ldap_username_field = models.CharField(max_length=191, blank=True, null=True)
    ldap_lname_field = models.CharField(max_length=191, blank=True, null=True)
    ldap_fname_field = models.CharField(max_length=191, blank=True, null=True)
    ldap_auth_filter_query = models.CharField(max_length=191, blank=True, null=True)
    ldap_version = models.IntegerField(blank=True, null=True)
    ldap_active_flag = models.CharField(max_length=191, blank=True, null=True)
    ldap_emp_num = models.CharField(max_length=191, blank=True, null=True)
    ldap_email = models.CharField(max_length=191, blank=True, null=True)
    full_multiple_companies_support = models.IntegerField()
    ldap_server_cert_ignore = models.IntegerField()
    locale = models.CharField(max_length=5, blank=True, null=True)
    labels_per_page = models.IntegerField()
    labels_width = models.DecimalField(max_digits=6, decimal_places=5)
    labels_height = models.DecimalField(max_digits=6, decimal_places=5)
    labels_pmargin_left = models.DecimalField(max_digits=6, decimal_places=5)
    labels_pmargin_right = models.DecimalField(max_digits=6, decimal_places=5)
    labels_pmargin_top = models.DecimalField(max_digits=6, decimal_places=5)
    labels_pmargin_bottom = models.DecimalField(max_digits=6, decimal_places=5)
    labels_display_bgutter = models.DecimalField(max_digits=6, decimal_places=5)
    labels_display_sgutter = models.DecimalField(max_digits=6, decimal_places=5)
    labels_fontsize = models.IntegerField()
    labels_pagewidth = models.DecimalField(max_digits=7, decimal_places=5)
    labels_pageheight = models.DecimalField(max_digits=7, decimal_places=5)
    labels_display_name = models.IntegerField()
    labels_display_serial = models.IntegerField()
    labels_display_tag = models.IntegerField()
    alt_barcode = models.CharField(max_length=191, blank=True, null=True)
    alt_barcode_enabled = models.IntegerField(blank=True, null=True)
    alert_interval = models.IntegerField(blank=True, null=True)
    alert_threshold = models.IntegerField(blank=True, null=True)
    email_domain = models.CharField(max_length=191, blank=True, null=True)
    email_format = models.CharField(max_length=191, blank=True, null=True)
    username_format = models.CharField(max_length=191, blank=True, null=True)
    is_ad = models.IntegerField()
    ad_domain = models.CharField(max_length=191, blank=True, null=True)
    ldap_port = models.CharField(max_length=5)
    ldap_tls = models.IntegerField()
    zerofill_count = models.IntegerField()
    ldap_pw_sync = models.IntegerField()
    two_factor_enabled = models.IntegerField(blank=True, null=True)
    require_accept_signature = models.IntegerField()
    date_display_format = models.CharField(max_length=191)
    time_display_format = models.CharField(max_length=191)
    next_auto_tag_base = models.BigIntegerField()
    login_note = models.TextField(blank=True, null=True)
    thumbnail_max_h = models.IntegerField(blank=True, null=True)
    pwd_secure_uncommon = models.IntegerField()
    pwd_secure_complexity = models.CharField(max_length=191, blank=True, null=True)
    pwd_secure_min = models.IntegerField()
    audit_interval = models.IntegerField(blank=True, null=True)
    audit_warning_days = models.IntegerField(blank=True, null=True)
    show_url_in_emails = models.IntegerField()
    custom_forgot_pass_url = models.CharField(max_length=191, blank=True, null=True)
    show_alerts_in_menu = models.IntegerField()
    labels_display_company_name = models.IntegerField()
    show_archived_in_list = models.IntegerField()
    dashboard_message = models.TextField(blank=True, null=True)
    support_footer = models.CharField(max_length=5, blank=True, null=True)
    footer_text = models.TextField(blank=True, null=True)
    modellist_displays = models.CharField(max_length=191, blank=True, null=True)
    login_remote_user_enabled = models.IntegerField()
    login_common_disabled = models.IntegerField()
    login_remote_user_custom_logout_url = models.CharField(max_length=191)
    skin = models.CharField(max_length=191, blank=True, null=True)
    show_images_in_email = models.IntegerField()
    admin_cc_email = models.CharField(max_length=191, blank=True, null=True)
    labels_display_model = models.IntegerField()
    privacy_policy_link = models.CharField(max_length=191, blank=True, null=True)
    version_footer = models.CharField(max_length=5, blank=True, null=True)
    unique_serial = models.IntegerField()
    logo_print_assets = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'settings'


class StatusLabels(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    deployable = models.IntegerField()
    pending = models.IntegerField()
    archived = models.IntegerField()
    notes = models.TextField(blank=True, null=True)
    color = models.CharField(max_length=10, blank=True, null=True)
    show_in_nav = models.IntegerField()
    default_label = models.IntegerField()

    class Meta:
        managed = False
        db_table = 'status_labels'


class Suppliers(models.Model):
    name = models.CharField(max_length=191)
    address = models.CharField(max_length=50, blank=True, null=True)
    address2 = models.CharField(max_length=50, blank=True, null=True)
    city = models.CharField(max_length=191, blank=True, null=True)
    state = models.CharField(max_length=32, blank=True, null=True)
    country = models.CharField(max_length=2, blank=True, null=True)
    phone = models.CharField(max_length=35, blank=True, null=True)
    fax = models.CharField(max_length=35, blank=True, null=True)
    email = models.CharField(max_length=150, blank=True, null=True)
    contact = models.CharField(max_length=100, blank=True, null=True)
    notes = models.CharField(max_length=191, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    user_id = models.IntegerField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    zip = models.CharField(max_length=10, blank=True, null=True)
    url = models.CharField(max_length=250, blank=True, null=True)
    image = models.CharField(max_length=191, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'suppliers'


class Throttle(models.Model):
    user_id = models.PositiveIntegerField(blank=True, null=True)
    ip_address = models.CharField(max_length=191, blank=True, null=True)
    attempts = models.IntegerField()
    suspended = models.IntegerField()
    banned = models.IntegerField()
    last_attempt_at = models.DateTimeField(blank=True, null=True)
    suspended_at = models.DateTimeField(blank=True, null=True)
    banned_at = models.DateTimeField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'throttle'


class Users(models.Model):
    email = models.CharField(max_length=191, blank=True, null=True)
    password = models.CharField(max_length=191)
    permissions = models.TextField(blank=True, null=True)
    activated = models.IntegerField()
    activation_code = models.CharField(max_length=191, blank=True, null=True)
    activated_at = models.DateTimeField(blank=True, null=True)
    last_login = models.DateTimeField(blank=True, null=True)
    persist_code = models.CharField(max_length=191, blank=True, null=True)
    reset_password_code = models.CharField(max_length=191, blank=True, null=True)
    first_name = models.CharField(max_length=191, blank=True, null=True)
    last_name = models.CharField(max_length=191, blank=True, null=True)
    created_at = models.DateTimeField(blank=True, null=True)
    updated_at = models.DateTimeField(blank=True, null=True)
    deleted_at = models.DateTimeField(blank=True, null=True)
    website = models.CharField(max_length=191, blank=True, null=True)
    country = models.CharField(max_length=191, blank=True, null=True)
    gravatar = models.CharField(max_length=191, blank=True, null=True)
    location_id = models.IntegerField(blank=True, null=True)
    phone = models.CharField(max_length=191, blank=True, null=True)
    jobtitle = models.CharField(max_length=191, blank=True, null=True)
    manager_id = models.IntegerField(blank=True, null=True)
    employee_num = models.TextField(blank=True, null=True)
    avatar = models.CharField(max_length=191, blank=True, null=True)
    username = models.CharField(max_length=191, blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    company_id = models.PositiveIntegerField(blank=True, null=True)
    remember_token = models.TextField(blank=True, null=True)
    ldap_import = models.IntegerField()
    locale = models.CharField(max_length=10, blank=True, null=True)
    show_in_list = models.IntegerField()
    two_factor_secret = models.CharField(max_length=32, blank=True, null=True)
    two_factor_enrolled = models.IntegerField()
    two_factor_optin = models.IntegerField()
    department_id = models.IntegerField(blank=True, null=True)
    address = models.CharField(max_length=191, blank=True, null=True)
    city = models.CharField(max_length=191, blank=True, null=True)
    state = models.CharField(max_length=3, blank=True, null=True)
    zip = models.CharField(max_length=10, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'users'


class UsersGroups(models.Model):
    user_id = models.PositiveIntegerField(primary_key=True)
    group_id = models.PositiveIntegerField()

    class Meta:
        managed = False
        db_table = 'users_groups'
        unique_together = (('user_id', 'group_id'),)
